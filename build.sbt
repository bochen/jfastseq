
lazy val root = (project in file(".")).
  settings(
    name := "jfastseq",
    version := "0.1",
    scalaVersion := "2.11.8",
    test in assembly := {},
    testOptions in Test := Seq(Tests.Filter(s => !s.contains("Redis"))),
    mainClass in Compile := Some("net.jfastseq.Main")

  ).settings(
  assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false)
).settings(
  fork in Test := true,
  parallelExecution in Test := false
).enablePlugins(JmhPlugin)

scalacOptions += "-target:jvm-1.8"

crossPaths := false
autoScalaLibrary := false

sourceDirectory in Jmh := (sourceDirectory in Test).value
classDirectory in Jmh := (classDirectory in Test).value
dependencyClasspath in Jmh := (dependencyClasspath in Test).value
compile in Jmh <<= (compile in Jmh) dependsOn (compile in Test)
run in Jmh <<= (run in Jmh) dependsOn (Keys.compile in Jmh)

javaOptions ++= Seq("-Xms2G", "-Xmx8G", "-XX:+CMSClassUnloadingEnabled")
javacOptions ++= Seq("-source", "1.8", "-target", "1.8")
scalacOptions += "-target:jvm-1.7"

resolvers += "jcenter.bintray.com" at "http://jcenter.bintray.com/"

libraryDependencies ++= Seq(
  "commons-io" % "commons-io" % "1.3.2",
  //"de.greenrobot" % "java-common" % "2.3.1",
  //"org.rocksdb" % "rocksdbjni" % "5.14.2",
  "info.picocli" % "picocli" % "3.9.5",
  "commons-cli" % "commons-cli" % "1.4",
  "commons-io" % "commons-io" % "2.6",
  "com.github.luben" % "zstd-jni" % "1.3.8-5",
  //"de.ruedigermoeller" % "fst" % "2.57",
  "com.esotericsoftware" % "kryo" % "5.0.0-RC2",
  "com.google.protobuf" % "protobuf-java" % "3.7.0-rc1",
  "redis.clients" % "jedis" % "3.0.1",
  "com.fasterxml.jackson.core" % "jackson-core" % "2.9.8",
  "com.fasterxml.jackson.core" % "jackson-databind" % "2.9.8",
  "org.slf4j" % "slf4j-simple" % "1.7.21" ,
  "org.slf4j" % "slf4j-api" % "1.7.21" ,
  "org.javatuples" % "javatuples" % "1.2",

  "junit" % "junit" % "4.12" % Test,

  "org.openjdk.jmh" % "jmh-core" % "1.21" % "provided"
)

excludeDependencies += "ch.qos.logback" % "logback-classic"

PB.targets in Compile := Seq(
  scalapb.gen(grpc = true, flatPackage = true) -> (sourceManaged in Compile).value
)

spIgnoreProvided := true
//spDependencies += "graphframes/graphframes:0.5.0-spark2.0-s_2.11"

assemblyMergeStrategy in assembly := {
  case PathList("javax", "servlet", xs@_*) => MergeStrategy.first
  case PathList(ps@_*) if ps.last endsWith ".html" => MergeStrategy.first
  case PathList("META-INF", xs@_*) => MergeStrategy.discard
  case "application.conf" => MergeStrategy.concat
  case "unwanted.txt" => MergeStrategy.discard
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}
