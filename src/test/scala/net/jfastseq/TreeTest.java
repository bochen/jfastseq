package net.jfastseq;

import org.junit.Test;

import java.util.Arrays;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class TreeTest {

    @Test
    public void test_load() throws Exception {
        Map<Integer, TreeNode> tree = Tree.readTree("data/ncbi_tree.json.gz");
        System.out.println(tree.size());
        System.out.println(tree.get(1));
        System.out.println(tree.get(2));
        System.out.println(tree.get(741158));
    }


    @Test
    public void test_tree() throws Exception {
        Tree tree = new Tree("data/ncbi_tree.json.gz");
        System.out.println("tree size " + tree.size());
        int nid = 1;
        System.out.println("node " + nid);
        System.out.println(Arrays.toString(tree.getCode(nid).toArray()));
        System.out.println(Arrays.toString(tree.getPath(nid).toArray()));
        System.out.println(Arrays.toString(tree.getNamePath(nid).toArray()));
        System.out.println(Arrays.toString(tree.getRankPath(nid).toArray()));

        nid = 2;
        System.out.println("node " + nid);
        System.out.println(Arrays.toString(tree.getCode(nid).toArray()));
        System.out.println(Arrays.toString(tree.getPath(nid).toArray()));
        System.out.println(Arrays.toString(tree.getNamePath(nid).toArray()));
        System.out.println(Arrays.toString(tree.getRankPath(nid).toArray()));

        nid = 741158;
        System.out.println("node " + nid);
        System.out.println(Arrays.toString(tree.getCode(nid).toArray()));
        System.out.println(Arrays.toString(tree.getPath(nid).toArray()));
        System.out.println(Arrays.toString(tree.getNamePath(nid).toArray()));
        System.out.println(Arrays.toString(tree.getRankPath(nid).toArray()));

        nid = 269798;
        System.out.println("node " + nid);
        System.out.println(Arrays.toString(tree.getCode(nid).toArray()));
        System.out.println(Arrays.toString(tree.getPath(nid).toArray()));
        System.out.println(Arrays.toString(tree.getNamePath(nid).toArray()));
        System.out.println(Arrays.toString(tree.getRankPath(nid).toArray()));

    }

}