package net.jfastseq;

import com.google.protobuf.InvalidProtocolBufferException;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.Assert.*;

public class ReadMsgProtosTest {

    @Test
    public void testMakeMessage() throws InvalidProtocolBufferException {
        ReadMsgProtos.Read read = ReadMsgProtos.Read.newBuilder().setLabel(123).addKmer(1).addKmer(2).build();
        assertTrue(read.hasLabel());
        assertEquals(123, read.getLabel());
        assertEquals(2, read.getKmerCount());
        assertEquals(1, read.getKmer(0));
        assertEquals(2, read.getKmer(1));

        byte[] bytes = read.toByteArray();

        ReadMsgProtos.Read read2 = ReadMsgProtos.Read.parseFrom(bytes);
        assertEquals(read, read2);
        assertEquals(123, read2.getLabel());
        assertEquals(2, read2.getKmerCount());
        assertEquals(1, read2.getKmer(0));
        assertEquals(2, read2.getKmer(1));
    }

    @Test
    public void testMakeMessage_2() throws InvalidProtocolBufferException {
        ReadMsgProtos.Read read = ReadMsgProtos.Read.newBuilder().build();
        assertTrue(!read.hasLabel());
        assertEquals(0, read.getKmerCount());
        byte[] bytes = read.toByteArray();

        ReadMsgProtos.Read read2 = ReadMsgProtos.Read.parseFrom(bytes);
        assertEquals(read, read2);
    }

    @Test
    public void testLoadFromPyEmpty() throws IOException {
        FileInputStream in = new FileInputStream("data/test_proto_msg_0.bin");
        ReadMsgProtos.Read read = ReadMsgProtos.Read.parseFrom(in);
        assertTrue(!read.hasLabel());
        assertEquals(0, read.getKmerCount());
        in.close();
    }

    @Test
    public void testLoadFromPy() throws IOException {
        FileInputStream in = new FileInputStream("data/test_proto_msg_1.bin");
        ReadMsgProtos.Read read2 = ReadMsgProtos.Read.parseFrom(in);
        assertEquals(1, read2.getLabel());
        assertEquals(3, read2.getKmerCount());
        assertEquals(1, read2.getKmer(0));
        assertEquals(2, read2.getKmer(1));
        assertEquals(3, read2.getKmer(2));
        in.close();
    }

}