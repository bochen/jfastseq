package net.jfastseq;

import org.junit.Test;

import static org.junit.Assert.*;

public class UtilsTest {

    @Test
    public void test_base64() {

        long n;
        String str;

        n = 0;
        str = Utils.ulong2base64(n);
        assertEquals(str, "A");
        assertEquals(n, Utils.base64toulong(str));

        n = 12358;
        str = Utils.ulong2base64(n);
        assertEquals(n, Utils.base64toulong(str));

        n = 3223212358L;
        str = Utils.ulong2base64(n);
        assertEquals(n, Utils.base64toulong(str));

        n = 83223212358L;
        str = Utils.ulong2base64(n);
        assertEquals(n, Utils.base64toulong(str));

    }
}
