package net.jfastseq;

import org.junit.Test;

import java.util.Arrays;
import java.util.Map;

public class ClfTrainTest {


    @Test
    public void test_tree() throws Exception {
        String[] argv = "train --input data/sample_hashed_?? --output data/model --tree data/ncbi_tree.json.gz --lr 0.4 --epoch 3 --dim 50 --thread 2 ".trim().split(" ");
        Main.main(argv);
    }

}