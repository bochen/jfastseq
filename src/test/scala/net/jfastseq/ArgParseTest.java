package net.jfastseq;

import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class ArgParseTest {

    @Test
    public void parse_train_args() throws Exception {


        {
            String[] argv = "--input data/dummydata_a.1 data/dummydata_a.2 --output model --lr 0.4 --epoch 3 --dim 50 ".trim().split(" ");
            Map<String, Object> args = new Trainer().parse_args(argv);
            assertEquals("model", args.get("output_folder"));
            assertEquals(0.4, (Double) args.get("lr"), 1e-10);
            assertEquals(3L, args.get("epoch"));
            assertEquals(50L, args.get("dim"));
            String[] files = (String[]) args.get("input_files");
            assertEquals(2L, files.length);
        }

        {
            String[] argv = "-i data/dummydata_*.* -o model --lr 0.4 --epoch 3 --dim 50 --echoPeriod 20 ".trim().split(" ");
            Args args = new Trainer().parse_args(argv);
            assertEquals("model", args.get("output_folder"));
            assertEquals(0.4, (double) args.get("lr"), 1e-10);
            assertEquals(3L, args.get("epoch"));
            assertEquals(50L, args.get("dim"));
            assertEquals(20, args.getEchoPeriod());
            System.out.println(args.get("input_files"));
            String[] files = (String[]) args.get("input_files");
            assertEquals(4, files.length);
        }

    }


}