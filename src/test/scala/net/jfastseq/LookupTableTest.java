package net.jfastseq;

import org.junit.Test;

import static org.junit.Assert.*;

public class LookupTableTest {

    @Test
    public void test_get_row() {
        LookupTable lt = new LookupTable(100);
        float[] row = lt.get(1);
        for (float v : row) {
            assertEquals(0.01f, v, 1e-12);
        }
    }

    @Test
    public void test_get_xy() {
        LookupTable lt = new LookupTable(100);
        float v = lt.get(12, 30);
        assertEquals(0.01f, v, 1e-12);
    }

    @Test
    public void test_contains1() {
        LookupTable lt = new LookupTable(100);
        assertTrue(lt.contains(12));
        float v = lt.get(12, 30);
        assertTrue(lt.contains(12));

    }

    @Test
    public void test_contains2() {
        LookupTable lt = new LookupTable(100);
        assertTrue(lt.contains(12));
        lt.get(222, 30);
        lt.setAllowInsert(false);
        assertFalse(lt.contains(12));
        assertTrue(lt.contains(222));
    }

    @Test
    public void test_remove1() {
        LookupTable lt = new LookupTable(100);
        lt.get(222, 30);
        lt.setAllowInsert(false);
        assertTrue(lt.contains(222));
        lt.remove(222);
        assertFalse(lt.contains(222));
    }

}
