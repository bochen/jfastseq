package net.jfastseq;

import net.sparc.sparc;
import net.sparc.stringVector;
import org.apache.commons.cli.*;

import java.io.IOException;
import java.util.Objects;

public class ToolEmbedding {
    public ToolEmbedding() {
    }


    private void run_embed_reads(String hashFile, LookupTable inputMap, String input, String output) throws IOException {
        net.sparc.Info.load_native();
        sparc.lsh_load_from_file(hashFile);
        int kmerSize = sparc.lsh_get_kmer_size();
        UniversalFileReader reader = null;
        UniversalFileWriter writer = null;
        try {
            Vector vec = new Vector(inputMap.dim);
            reader = new UniversalFileReader(input);
            writer = new UniversalFileWriter(output);
            String line;
            while ((line = reader.readLine()) != null) {
                String[] array = line.split("\t");
                if (array.length != 3) {
                    System.out.println("Format error! Skip line: " + line);
                    continue;
                }
                String id = array[1];
                String seq = array[2];
                stringVector kmers = sparc.generate_kmer_for_fastseq(seq, kmerSize, 'N', true);
                int[] words = new int[(int) kmers.size()];
                for (int i = 0; i < kmers.size(); i++) {
                    int a = (int) sparc.lsh_hash_kmer(kmers.get(i), true);
                    words[i] = a;
                }
                Record r = new Record(words);

                StringBuffer builder = new StringBuffer();
                builder.append(id).append(" ");
                vec.zero();
                for (int word : r.getWords()) {
                    vec.addRow(inputMap, word);

                }
                vec.divide(r.size());
                for (int i = 0; i < vec.size(); i++) {
                    builder.append(String.format("%.5f", vec.get(i))).append(" ");
                }
                writer.writeLine(builder.toString());


            }
        } finally {
            Objects.requireNonNull(writer).close();
            Objects.requireNonNull(reader).close();
        }

    }


    public void run_Embedding(String[] argv) throws Exception {
        Option help = new Option("help", "print this message");
        Options options = new Options();
        Utils.create_option("input", "input file", String.class, options, true);
        Utils.create_option("output", "output file", String.class, options, true);
        Utils.create_option("inputModel", "input model", String.class, options, true);
        Utils.create_option("hash", "hash function. If hash function is provided, assume input is a seq file", String.class, options, true);

        options.addOption(help);

        CommandLineParser parser = new DefaultParser();
        CommandLine cmdline = null;

        try {
            cmdline = parser.parse(options, argv);

        } catch (Exception e) {
            e.printStackTrace();
            Utils.printUsage(this.getClass().getName(), options);
            System.exit(-1);
            return;
        }

        if (cmdline.hasOption("help")) {
            Utils.printUsage(this.getClass().getName(), options);
            return;
        }

        String input = Utils.getStringOption(cmdline, "input");
        String output = Utils.getStringOption(cmdline, "output");
        String inputModel = Utils.getStringOption(cmdline, "inputModel");
        String hashFile = Utils.getStringOption(cmdline, "hash");

        EmbeddingModel model = EmbeddingCont.makeModel(inputModel);

        LookupTable inputMap = model.wi_;

        run_embed_reads(hashFile, inputMap, input, output);

    }


}
