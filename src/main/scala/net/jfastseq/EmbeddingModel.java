package net.jfastseq;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class EmbeddingModel {


    public final static int MAX_SIGMOID = 8;
    public final static int SIGMOID_TABLE_SIZE = 512;
    public final static int LOG_TABLE_SIZE = 512;

    public final static float[] t_sigmoid_;
    public final static float[] t_log_;

    static {
        t_sigmoid_ = new float[SIGMOID_TABLE_SIZE + 1];
        for (int i = 0; i < SIGMOID_TABLE_SIZE + 1; i++) {
            float x = (1.0f * i * 2 * MAX_SIGMOID) / SIGMOID_TABLE_SIZE - MAX_SIGMOID;
            t_sigmoid_[i] = (float) (1.0 / (1.0 + Math.exp(-x)));
        }
    }

    static {
        t_log_ = new float[LOG_TABLE_SIZE + 1];
        for (int i = 0; i < LOG_TABLE_SIZE + 1; i++) {
            float x = (float) (((float) (i) + 1e-5) / LOG_TABLE_SIZE);
            t_log_[i] = (float) Math.log(x);
        }
    }


    private final int dim;
    protected final LookupTable wi_;
    private final WordCounter wc;
    private final boolean update_wc;
    private final int neg_size;
    private final boolean need_update;
    private final ThreadLocalRandom threadlocal;
    private final boolean use_cbow;
    Vector hidden_;
    Vector grad_;
    LookupTable wo_;
    float loss_ = 0f;
    int count = 0;
    private final ArrayList<Integer> negatives_;
    private ArrayList<Integer> input_words = new ArrayList<>();
    private int half_window;


    public EmbeddingModel(int dim, LookupTable wi_, LookupTable wo_, int neg_size, WordCounter wc, ArrayList<Integer> negatives, int half_window, boolean use_cbow) {
        this.dim = dim;
        this.wc = wc;
        this.neg_size = neg_size;
        this.half_window = half_window;
        this.use_cbow = use_cbow;
        assert (half_window > 0);
        this.negatives_ = negatives;
        this.need_update = negatives != null && negatives.size() >= 1000;
        this.update_wc = wc != null;
        hidden_ = new Vector(dim);
        grad_ = new Vector(dim);
        this.wi_ = wi_;
        this.wo_ = wo_;
        threadlocal = ThreadLocalRandom.current();

    }


    public void update(ReadMsgProtos.Read record, float lr) throws Exception {

        List<Integer> words = record.getKmerList();
        for (int i = 0; i < words.size(); i++) {
            Integer label = words.get(i);
            if (use_cbow) {
                input_words.clear();
                for (int j = i - half_window; j < i + half_window; j++) {
                    if (j >= 0 && j != i && j < words.size()) {
                        input_words.add(words.get(j));
                    }
                }
                update(label, input_words, lr);
            } else {
                for (int j = i - half_window; j < i + half_window; j++) {
                    if (j >= 0 && j != i && j < words.size()) {
                        input_words.clear();
                        input_words.add(words.get(j));
                        update(label, input_words, lr);
                    }

                }
            }

        }
    }


    public void update(int label, List<Integer> words, float lr) throws Exception {
        if (words.isEmpty()) return;
        if (update_wc) {
            wc.incr(label);
            for (int i = 0; i < words.size(); i++) {
                int word = words.get(i);
                wc.incr(word);
            }
        }
        if (need_update) {
            computeHidden(words, hidden_);
            loss_ += negativeSampling(label, lr);
            count++;
            for (int i = 0; i < words.size(); i++) {
                int word = words.get(i);
                if (wi_.contains(word)) {
                    wi_.addRow(grad_, word, 1.0f);
                }
            }
        }
    }


    public float calcLoss(ReadMsgProtos.Read record) throws Exception {
        if (negatives_.isEmpty()) {
            return 20;
        }

        List<Integer> words = record.getKmerList();
        if (update_wc) {
            for (int i = 0; i < words.size(); i++) {
                int word = words.get(i);
                wc.touch(word);
            }
        }

        float loss = 0.0f;
        int count = 0;
        for (int i = 0; i < words.size(); i++) {
            if (use_cbow) {
                Integer label = words.get(i);
                input_words.clear();

                for (int j = i - half_window; j < i + half_window; j++) {
                    if (j >= 0 && j != i && j < words.size()) {
                        input_words.add(words.get(j));
                    }
                }
                loss += calcLoss(label, input_words);
                count++;
            } else {
                Integer label = words.get(i);

                for (int j = i - half_window; j < i + half_window; j++) {
                    if (j >= 0 && j != i && j < words.size()) {
                        input_words.clear();
                        input_words.add(words.get(j));
                        loss += calcLoss(label, input_words);
                        count++;
                    }

                }
            }

        }
        return loss / count;
    }


    private float calcLoss(Integer label, ArrayList<Integer> input_words) throws Exception {
        if (input_words.isEmpty()) return 0;
        computeHidden(input_words, hidden_);

        grad_.zero();
        float loss = 0;
        for (int n = 0; n <= neg_size; n++) {
            if (n == 0) {
                loss += binaryLogistic(label, true);
            } else {
                loss += binaryLogistic(getNegative(label), false);
            }
        }
        return loss;
    }


    protected float negativeSampling(int target, float lr) throws Exception {
        float loss = 0.0f;
        grad_.zero();
        for (int n = 0; n <= neg_size; n++) {
            if (n == 0) {
                loss += binaryLogistic(target, true, lr);
            } else {
                loss += binaryLogistic(getNegative(target), false, lr);
            }
        }
        return loss;
    }

    private Integer getNegative(int target) {
        int negative;
        int idx;

        do {
            idx = threadlocal.nextInt(negatives_.size());
            negative = negatives_.get(idx);
        } while (target == negative);
        return negative;
    }

    private float binaryLogistic(Integer label, Boolean ytruth, float lr) throws Exception {
        float score = sigmoid(wo_.dotRow(hidden_, label));
        float alpha = lr * ((ytruth ? 1f : 0f) - score);
        grad_.addRow(wo_, label, alpha);
        wo_.addRow(hidden_, label, alpha);
        if (ytruth) {
            return -log(score);
        } else {
            return -log(1.0f - score);
        }
    }

    private float binaryLogistic(Integer label, Boolean ytruth) throws Exception {
        float score = sigmoid(wo_.dotRow(hidden_, label));
        if (ytruth) {
            return -log(score);
        } else {
            return -log(1.0f - score);
        }
    }


    private void computeHidden(List<Integer> words, Vector hidden) {
        hidden.zero();
        int n = 0;
        for (int i = 0; i < words.size(); i++) {
            if (wi_.contains(words.get(i))) {
                n++;
                hidden.addRow(wi_, words.get(i));
            }
        }
        if (n > 0) {
            hidden.mul(1.0f / n);
        }
    }

    float sigmoid(float x) {
        if (x < -MAX_SIGMOID) {
            return 0.0f;
        } else if (x > MAX_SIGMOID) {
            return 1.0f;
        } else {
            int i = (int) ((x + MAX_SIGMOID) * SIGMOID_TABLE_SIZE / MAX_SIGMOID / 2);
            return t_sigmoid_[i];
        }
    }

    float log(float x) {
        if (x > 1.0) {
            return 0.0f;
        }
        int i = (int) (x * LOG_TABLE_SIZE);
        return t_log_[i];
    }

    public float avgLoss() {
        return loss_ / count;
    }

    public void zeroLoss() {
        loss_ = 0f;
        count = 0;
    }


    private float std_log(float x) {
        return (float) Math.log(x + 1e-5);
    }


}
