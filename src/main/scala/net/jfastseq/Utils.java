package net.jfastseq;


import com.esotericsoftware.wildcard.Paths;
import org.apache.commons.cli.*;

import java.io.File;

import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;


public class Utils {
    public static boolean path_exists(String filePathString, boolean is_dir) {
        File f = new File(filePathString);
        if (is_dir) {
            return f.exists() && f.isDirectory();
        } else {
            return f.exists() && f.isFile();
        }
    }

    public static String get_cwd() {
        return System.getProperty("user.dir");
    }

    public static void create_folder(String dirpath) {
        new File(dirpath).mkdirs();

    }

    public static String[] findFiles(String[] patterns) {

        Set<String> set = new HashSet<>();

        for (int i = 0; i < patterns.length; i++) {
            String[] a = findFile(patterns[i]);
            set.addAll(Arrays.asList(a));
        }

        return ArrayUtils.toArray(set);

    }

    public static String[] findFile(String pattern) {
        ArrayList<String> ret = new ArrayList<>();
        com.esotericsoftware.wildcard.Paths paths = new com.esotericsoftware.wildcard.Paths();

        Paths lst = paths.glob(".", pattern);
        Iterator<File> itor = lst.fileIterator();
        while (itor.hasNext()) {
            ret.add(itor.next().toString());
        }
        return ArrayUtils.toArray(ret);
    }

    private static char[] BASE64_CHARS;


    private static int[] CHARS_BASE64 = new int[255];

    static {
        String str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                "abcdefghijklmnopqrstuvwxyz" +
                "0123456789+/";
        BASE64_CHARS = str.toCharArray();
        for (int i = 0; i < BASE64_CHARS.length; i++) {
            char c = BASE64_CHARS[i];
            CHARS_BASE64[c] = i;
        }
    }


    public static long base64toulong(String str) {
        long n = 0;
        for (int i = 0; i < str.length(); i++) {
            n = n << 6;
            char c = str.charAt(i);
            n += CHARS_BASE64[c];
        }
        return n;
    }

    public static String ulong2base64(long n) {
        if (n == 0)
            return "A";
        StringBuilder strBuilder = new StringBuilder();

        int r;
        while (n != 0) {
            r = (int) (n % 64);
            strBuilder.append(BASE64_CHARS[r]);
            n = n >> 6;
        }
        return strBuilder.reverse().toString();
    }

    public static void create_option(String name, String description, Class claz, Options options) {
        create_option(name, description, claz, options, false);
    }

    public static void create_option(String name, String description, Class claz, Options options, boolean required) {
        Option.Builder builder = Option.builder().argName(name)
                .longOpt(name).required(required)
                .type(claz)
                .desc(description);

        if (claz != Boolean.class) {
            builder = builder.hasArg();
        }
        Option opt = builder.build();
        options.addOption(opt);
    }

    public static void update_args(CommandLine line, String name, Args arg) throws ParseException {
        update_args(line, name, arg, null);
    }

    public static void update_args(CommandLine line, String name, Args args, Object defaultValue) throws ParseException {
        Object v;
        if (line.hasOption(name) && (v = line.getParsedOptionValue(name)) != null) {
            args.put(name, v);
        } else {
            if (defaultValue != null) {
                args.put(name, defaultValue);
            }
        }

    }

    public static String getStringOption(CommandLine line, String name) {
        return getStringOption(line, name, null);

    }

    public static String getStringOption(CommandLine line, String name, String defaultValue) {
        if (line.hasOption(name)) {
            return line.getOptionValue(name);
        } else {
            return defaultValue;
        }
    }

    public static int getIntegerOption(CommandLine line, String name, int defaultValue) {
        if (line.hasOption(name)) {
            return Integer.valueOf(line.getOptionValue(name));
        } else {
            return defaultValue;
        }
    }

    public static float getFloatOption(CommandLine line, String name, float defaultValue) {
        if (line.hasOption(name)) {
            return Float.valueOf(line.getOptionValue(name));
        } else {
            return defaultValue;
        }
    }

    public static boolean getBooleanOption(CommandLine line, String name, boolean defaultValue) {
        if (line.hasOption(name)) {
            return true;
        } else {
            return defaultValue;
        }
    }


    public static void printUsage(
            final String applicationName,
            final Options options) {
        final PrintWriter writer = new PrintWriter(System.err);
        final HelpFormatter usageFormatter = new HelpFormatter();
        usageFormatter.printUsage(writer, 80, applicationName, options);
        writer.flush();
    }
}
