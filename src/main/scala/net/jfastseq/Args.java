package net.jfastseq;

import java.util.HashMap;

public class Args extends HashMap<String, Object> {
    private static final long serialVersionUID = 36248320763181265L;

    private int toInt(Object o) {
        if (o instanceof String) {
            return Integer.valueOf((String) o);
        } else if (o instanceof Long) {
            return (int) (long) o;
        } else {
            return (int) o;
        }
    }

    private float toFloat(Object o) {
        if (o instanceof String) {
            return Float.valueOf((String) o);
        } else if (o instanceof Double) {
            return (float) (double) o;
        } else {
            return (float) o;
        }
    }

    public String[] input_files() {
        return (String[]) get("input_files");
    }

    public String getResumeModel() {
        Object a = get("inputModel");
        return a == null ? "" : (String) a;
    }

    public int getThread() {
        return toInt(get("thread"));
    }

    public long getSavePeriod() {
        Object a = get("savePeriod");
        return a == null ? 2 : (long) (int) toInt(a);
    }

    public int getEstimatedWordSize() {
        Object a = get("estimatedWordSize");
        return a == null ? 20_000_000 : toInt(a);

    }

    public int getEstimatedLabelSize() {
        Object a = get("estimatedLabelSize");
        return a == null ? 3_000_000 : toInt(a);
    }

    public long getEchoPeriod() {
        Object a = get("echoPeriod");
        return a == null ? 10000 : (long) (int) toInt(a);
    }

    public int getDim() {
        return toInt(get("dim"));
    }

    public int getEpoch() {
        return toInt(get("epoch"));
    }

    public float getLearningRate() {
        return toFloat(get("lr"));
    }

    public int getEpochSize() {
        if (!containsKey("epochSize")) {
            this.keySet().forEach(System.out::println);
            throw new RuntimeException("epochSize is not set");
        }
        String str = ((String) get("epochSize")).trim().toUpperCase();
        if (str.charAt(str.length() - 1) == 'K') {
            return Integer.valueOf(str.substring(0, str.length() - 1)) * 1_000;
        } else if (str.charAt(str.length() - 1) == 'M') {
            return Integer.valueOf(str.substring(0, str.length() - 1)) * 1_000_000;
        } else {
            return Integer.valueOf(str);
        }


    }

    public String getRedisHost() {
        return (String) get("address");
    }

    public int getRedisPort() {
        return toInt(get("port"));
    }

    public String getQueueName() {
        Object a = get("queue");
        return a == null ? "QUEUE" : (String) a;
    }

    public String getOutputFolder() {
        return (String) get("output_folder");
    }

    public String set(String key, String val) {
        if (!containsKey(key)) {
            return "Key " + key + " does not exist";
        }
        Object oldval = get(key);
        if (oldval == null) {
            return "Error! null value cannot be set.";
        }
        Class<?> claz = oldval.getClass();

        Object val2 = null;
        try {
            val2 = toVal(claz, val);
            put(key, val2);
            return String.format("set %s from %s to %s ", key, oldval.toString(), val2.toString());

        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    private Object toVal(Class<?> claz, String val) throws Exception {
        if (claz.equals(String.class)) {
            return val;
        } else if (claz.equals(Long.class)) {
            return Long.valueOf(val);
        } else if (claz.equals(Integer.class)) {
            return Integer.valueOf(val);
        } else if (claz.equals(Float.class)) {
            return Float.valueOf(val);
        } else if (claz.equals(Double.class)) {
            return Double.valueOf(val);
        } else {
            throw new Exception(claz.getCanonicalName() + " is not supported.");
        }
    }

    public void setEnableWordCount(boolean b) {
        this.put("enableWordCount", b);
    }

    public boolean getEnableWordCount() {
        if (!containsKey("enableWordCount")) {
            setEnableWordCount(true);
        }
        return (boolean) get("enableWordCount");
    }

    public boolean is_rcc() {
        return containsKey("rcc") && ((boolean) get("rcc"));
    }

    public int getMinQueueSize() {
        return toInt(get("minQueueSize"));
    }

    public int getAdminPort() {
        if (containsKey("adminPort")) {
            return toInt(get("adminPort"));
        } else {
            return 10511;
        }
    }

    public int getNegSize() {
        return toInt(get("negSize"));
    }

    public int getHalfWindow() {
        return toInt(get("halfWindow"));
    }

    public String getEmbeddingType() {
        String a = (String) get("embeddingType");
        if (a == null || (!a.toLowerCase().equals("cbow") && !a.toLowerCase().equals("skipgram"))) {
            throw new RuntimeException("unknown " + a);
        }
        return a.toLowerCase();
    }

    public boolean useCbow() {
        return "cbow".equals(getEmbeddingType());
    }
}
