package net.jfastseq;

import java.util.*;

public class HSModel {


    public static class PredPair implements Comparable<PredPair> {
        public final int label;
        public final float score;

        public PredPair(int label, float score) {
            this.label = label;
            this.score = score;
        }

        @Override
        public String toString() {
            return String.format("%s:%.4f", label, Math.exp(score));
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            PredPair predPair = (PredPair) o;
            return label == predPair.label &&
                    Float.compare(predPair.score, score) == 0;
        }

        @Override
        public int hashCode() {
            return Objects.hash(label, score);
        }

        @Override
        public int compareTo(PredPair o) {
            return Float.compare(this.score, o.score);
        }
    }

    public final static int MAX_SIGMOID = 8;
    public final static int SIGMOID_TABLE_SIZE = 512;
    public final static int LOG_TABLE_SIZE = 512;

    public final static float[] t_sigmoid_;
    public final static float[] t_log_;

    static {
        t_sigmoid_ = new float[SIGMOID_TABLE_SIZE + 1];
        for (int i = 0; i < SIGMOID_TABLE_SIZE + 1; i++) {
            float x = (1.0f * i * 2 * MAX_SIGMOID) / SIGMOID_TABLE_SIZE - MAX_SIGMOID;
            t_sigmoid_[i] = (float) (1.0 / (1.0 + Math.exp(-x)));
        }
    }

    static {
        t_log_ = new float[LOG_TABLE_SIZE + 1];
        for (int i = 0; i < LOG_TABLE_SIZE + 1; i++) {
            float x = (float) (((float) (i) + 1e-5) / LOG_TABLE_SIZE);
            t_log_[i] = (float) Math.log(x);
        }
    }


    private final int dim;
    protected final LookupTable wi_;
    private final WordCounter wc;
    private final boolean update_wc;
    Vector hidden_;
    Vector grad_;
    LookupTable wo_;
    float loss_ = 0f;
    int count = 0;
    Map<Integer, ArrayList<Integer>> paths;
    Map<Integer, ArrayList<Boolean>> codes;
    protected Tree tree;

    private int targetLabel = -1;

    public HSModel(int dim, LookupTable wi_, LookupTable wo_, Tree tree, WordCounter wc) {
        this.dim = dim;
        this.wc = wc;
        this.update_wc = wc != null;
        hidden_ = new Vector(dim);
        grad_ = new Vector(dim);
        this.wi_ = wi_;
        this.wo_ = wo_;
        this.tree = tree;
        this.paths = tree.getPaths();
        this.codes = tree.getCodes();
    }


    public void update(ReadMsgProtos.Read record, float lr) throws Exception {
        List<Integer> words = record.getKmerList();
        int label = record.getLabel();
        update(label, words, lr);
    }

    public void update(Record record, float lr) throws Exception {
        int[] words = record.getWords();
        Integer label = record.getLabel();
        update(label, words, lr);
    }

    public void update(int label, List<Integer> words, float lr) throws Exception {
        computeHidden(words, hidden_);
        loss_ += hierarchicalSoftmax(label, lr);
        grad_.mul(1.0f / words.size());
        for (int i = 0; i < words.size(); i++) {
            int word = words.get(i);
            if (wi_.contains(word)) {
                wi_.addRow(grad_, word, 1.0f);
                if (update_wc) {
                    wc.incr(word);
                }
            }
        }
    }

    public void update(int label, int[] words, float lr) throws Exception {
        computeHidden(words, hidden_);
        loss_ += hierarchicalSoftmax(label, lr);
        grad_.mul(1.0f / words.length);
        for (int i = 0; i < words.length; i++) {
            int word = words[i];
            if (wi_.contains(word)) {
                wi_.addRow(grad_, word, 1.0f);
                if (update_wc) {
                    wc.incr(word);
                }
            }
        }
    }

    public float calcLoss(ReadMsgProtos.Read record) throws Exception {
        List<Integer> words = record.getKmerList();
        int label = record.getLabel();
        computeHidden(words, hidden_);

        float loss = 0.0f;
        grad_.zero();
        ArrayList<Boolean> binaryCode = codes.get(label);
        ArrayList<Integer> pathToRoot = paths.get(label);

        for (int i = 0; i < pathToRoot.size(); i++) {
            loss += binaryLogistic(pathToRoot.get(i), binaryCode.get(i));
        }

        for (int i = 0; i < words.size(); i++) {
            wc.touch(words.get(i));
        }
        return loss;
    }


    private float hierarchicalSoftmax(int label, float lr) throws Exception {
        float loss = 0.0f;
        grad_.zero();
        ArrayList<Boolean> binaryCode = codes.get(label);
        ArrayList<Integer> pathToRoot = paths.get(label);

        for (int i = 0; i < pathToRoot.size(); i++) {
            loss += binaryLogistic(pathToRoot.get(i), binaryCode.get(i), lr);
        }
        count++;
        return loss;
    }

    private float binaryLogistic(Integer label, Boolean ytruth, float lr) throws Exception {
        float score = sigmoid(wo_.dotRow(hidden_, label));
        float alpha = lr * ((ytruth ? 1f : 0f) - score);
        grad_.addRow(wo_, label, alpha);
        wo_.addRow(hidden_, label, alpha);
        if (ytruth) {
            return -log(score);
        } else {
            return -log(1.0f - score);
        }
    }

    private float binaryLogistic(Integer label, Boolean ytruth) throws Exception {
        float score = sigmoid(wo_.dotRow(hidden_, label));
        if (ytruth) {
            return -log(score);
        } else {
            return -log(1.0f - score);
        }
    }

    private void computeHidden(int[] words, Vector hidden) {
        hidden.zero();
        int n = 0;
        for (int i = 0; i < words.length; i++) {
            if (wi_.contains(words[i])) {
                n++;
                hidden.addRow(wi_, words[i]);
            }
        }
        if (n > 0) {
            hidden.mul(1.0f / n);
        }
    }

    private void computeHidden(List<Integer> words, Vector hidden) {
        hidden.zero();
        int n = 0;
        for (int i = 0; i < words.size(); i++) {
            if (wi_.contains(words.get(i))) {
                n++;
                hidden.addRow(wi_, words.get(i));
            }
        }
        if (n > 0) {
            hidden.mul(1.0f / n);
        }
    }

    float sigmoid(float x) {
        if (x < -MAX_SIGMOID) {
            return 0.0f;
        } else if (x > MAX_SIGMOID) {
            return 1.0f;
        } else {
            int i = (int) ((x + MAX_SIGMOID) * SIGMOID_TABLE_SIZE / MAX_SIGMOID / 2);
            return t_sigmoid_[i];
        }
    }

    float log(float x) {
        if (x > 1.0) {
            return 0.0f;
        }
        int i = (int) (x * LOG_TABLE_SIZE);
        return t_log_[i];
    }

    public float avgLoss() {
        return loss_ / count;
    }

    public void zeroLoss() {
        loss_ = 0f;
        count = 0;
    }

    public PredPair[] predict(ReadMsgProtos.Read read, int k, float threshold, boolean onlyLeaf,
                              boolean withoutUncult, int targetLabel) throws Exception {
        this.targetLabel = targetLabel;
        int[] arr = new int[read.getKmerCount()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = read.getKmer(i);
        }
        Record r = new Record(arr);
        PredPair[] ret = predict(r, k, threshold, onlyLeaf, withoutUncult);
        Arrays.sort(ret, (o1, o2) -> -Float.compare(o1.score, o2.score));
        return ret;
    }


    public PredPair[] predict(Record r, int k, float threshold, boolean onlyLeaf, boolean withoutUncult) throws Exception {
        PriorityQueue<PredPair> heap = new PriorityQueue<>();
        Vector this_hidden = new Vector(dim);
        return predict(r, k, threshold, heap, this_hidden, onlyLeaf, withoutUncult);
    }

    private PredPair[] predict(Record r, int k, float threshold, PriorityQueue<PredPair> heap, Vector hidden, boolean onlyLeaf, boolean withoutUncult) throws Exception {
        computeHidden(r.getWords(), hidden);
        dfs(k, threshold, tree.getRootId(), 0.0f, heap, hidden, onlyLeaf, withoutUncult);
        PredPair[] a = heap.toArray(new PredPair[0]);
        return a;
    }

    private void dfs(int k, float threshold, int nid, float score, PriorityQueue<PredPair> heap,
                     Vector hidden, boolean onlyLeaf, boolean withoutUncult) throws Exception {
        if (score < std_log(threshold)) return;
        if (heap.size() >= k) {
            if (score < heap.peek().score) {
                return;
            } else {
                heap.poll();
            }
        }
        TreeNode node = tree.getNode(nid);
        if (!node.isDummy()) {
            boolean pass = true;
            if ((onlyLeaf && !node.isLeaf()) || (withoutUncult && tree.isUncult(nid))) {
                pass = false;
            }
            if (targetLabel == nid) {
                pass = true;
            }
            if (pass) {
                heap.add(new PredPair(nid, score));
            }
        }
        float f = wo_.dotRow(hidden, nid);
        f = (float) (1. / (1 + Math.exp(-f)));
        if (node.left > 0) {
            dfs(k, threshold, node.left, score + std_log(f), heap, hidden, onlyLeaf, withoutUncult);
        }
        if (node.right > 0) {
            dfs(k, threshold, node.right, score + std_log(1.0f - f), heap, hidden, onlyLeaf, withoutUncult);
        }
    }

    private float std_log(float x) {
        return (float) Math.log(x + 1e-5);
    }


}
