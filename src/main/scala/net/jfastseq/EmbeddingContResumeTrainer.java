package net.jfastseq;

import org.apache.commons.cli.*;
import org.slf4j.LoggerFactory;

public class EmbeddingContResumeTrainer {
    private final static org.slf4j.Logger logger = LoggerFactory.getLogger(EmbeddingContResumeTrainer.class);

    public void run(String[] argv) throws Exception {

        Args args = parse_args(argv);
        args.forEach((k, v) -> {
            logger.info(String.format("%s %s=%s", (v == null ? "null" : v.getClass().getName()), k, (v == null ? "null" : v.toString())));
        });
        new EmbeddingCont(args).train();
    }


    protected Args parse_args(String[] argv) throws Exception {
        Option help = new Option("help", "print this message");

        Option debug = new Option("debug", "print debugging information");

        Options options = new Options();
        Utils.create_option("thread", "number of threads", Number.class, options);
        Utils.create_option("address", "redis host", String.class, options);
        Utils.create_option("port", "redis port", Number.class, options);
        Utils.create_option("adminPort", "admin port", Number.class, options);
        Utils.create_option("epochSize", "epochSize", String.class, options);

        Utils.create_option("inputModel", "input model to resume training", String.class, options, true);


        options.addOption(help);
        options.addOption(debug);
        CommandLineParser parser = new DefaultParser();
        CommandLine line = parser.parse(options, argv);

        Args args = new Args();

        if (line.hasOption("thread")) {
            Integer thread = Integer.valueOf(line.getOptionValue("thread"));
            args.put("thread", thread);
        }

        if (line.hasOption("port")) {
            Integer a = Integer.valueOf(line.getOptionValue("port"));
            args.put("port", a);
        }

        if (line.hasOption("address")) {
            String a = line.getOptionValue("address");
            args.put("address", a);
        }

        if (line.hasOption("adminPort")) {
            String a = line.getOptionValue("adminPort");
            args.put("adminPort", a);
        }

        if (line.hasOption("epochSize")) {
            String a = line.getOptionValue("epochSize");
            args.put("epochSize", a);
        }

        String inputModel;
        if (line.hasOption("inputModel")) {
            inputModel = line.getOptionValue("inputModel");
        } else {
            throw new Exception("input model has to be specified.");
        }


        args.put("inputModel", inputModel);
        return args;
    }

}
