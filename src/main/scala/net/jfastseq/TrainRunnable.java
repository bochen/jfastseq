package net.jfastseq;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.logging.Logger;

public class TrainRunnable implements Runnable {

    private final int tid;
    private final Classifier classifier;
    private final float lr;
    private final ArrayList<String> files;
    private final HSModel model;
    private final Args args;
    private final boolean update_wc;
    private Logger logger;
    private long echo_per_n_read;

    public TrainRunnable(int tid, Classifier classifier, double lr, ArrayList<String> local_files, boolean update_wc) {
        this.tid = tid;
        this.update_wc=update_wc;
        this.classifier = classifier;
        this.lr = (float) lr;
        this.files = local_files;
        logger = MyFormatter.getLogger("TrainThread-" + tid);
        int dim = classifier.getArgs().getDim();
        this.args = classifier.getArgs();
        echo_per_n_read= args.getEchoPeriod();
        WordCounter wc = update_wc ? classifier.word_counts : null;
        this.model = new HSModel(dim, classifier.getInputWeights(), classifier.getOutputWeights(), classifier.getTree(), wc);
    }

    public void run() {
        long startTime = System.nanoTime();
        long total_num = 0;
        for (int i = 0; i < files.size(); i++) {
            String fileName = files.get(i);
            logger.info("Training " + fileName);
            long skipped = 0;
            long this_num = 0;
            try (UniversalFileReader br = new UniversalFileReader( fileName)) {
                for (String line; (line = br.readLine()) != null; ) {
                    String[] words = line.trim().split(" ");
                    if (words.length < 2) {
                        skipped++;
                    } else {
                        Record record = new Record(words);
                        model.update(record, this.lr);
                        this_num++;
                        total_num++;
                    }

                    if (tid == 0 && total_num % echo_per_n_read == 0 && total_num > 0) {
                        long endTime = System.nanoTime();
                        double duration = (endTime - startTime) * 1e-9;
                        double avg_rate = total_num / duration;
                        System.err.print(String.format("\r %.0f words/thread/second, loss=%.3f, lr=%.5f",
                                avg_rate,model.avgLoss(), lr));
                        model.zeroLoss();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
            System.err.println();
            logger.info(String.format("finish training %d records, skipping %d lines", this_num, skipped));
        }
        logger.info(String.format("Total trained %d words", total_num));
    }
}