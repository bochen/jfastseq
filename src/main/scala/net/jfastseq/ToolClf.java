package net.jfastseq;

import net.sparc.sparc;
import net.sparc.stringVector;
import org.apache.commons.cli.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;

import org.javatuples.Pair;


public class ToolClf {
    private static final int BATCH_SIZE = 1024 * 8;
    private static final HashSet<String> CAMI2_RANKS = new HashSet<>();

    static {
        CAMI2_RANKS.add("superkingdom");
        CAMI2_RANKS.add("phylum");
        CAMI2_RANKS.add("class");
        CAMI2_RANKS.add("order");
        CAMI2_RANKS.add("family");
        CAMI2_RANKS.add("genus");
        CAMI2_RANKS.add("species");
    }

    public ToolClf() {
    }

    public void run_predict(String[] argv) throws Exception {
        Option help = new Option("help", "print this message");
        Options options = new Options();
        Utils.create_option("input", "input file", String.class, options, true);
        Utils.create_option("output", "output file", String.class, options, true);
        Utils.create_option("inputModel", "input model", String.class, options, true);
        Utils.create_option("hash", "hash function. If hash function is provided, assume input is a seq file", String.class, options, false);
        Utils.create_option("max", "max number of labels", Number.class, options);
        Utils.create_option("thread", "number of threads to use", Number.class, options);
        Utils.create_option("threshold", "prob threshold to output", Number.class, options);
        Utils.create_option("showName", "show tax name instead of id", Boolean.class, options);
        Utils.create_option("onlyLeaf", "show only leaf taxid", Boolean.class, options);
        Utils.create_option("cami2", "show only main tax node", Boolean.class, options);
        Utils.create_option("withoutUncult", "without uncultured", Boolean.class, options);
        options.addOption(help);
        CommandLineParser parser = new DefaultParser();
        CommandLine cmdline = null;

        try {
            cmdline = parser.parse(options, argv);

        } catch (Exception e) {
            e.printStackTrace();
            Utils.printUsage(this.getClass().getName(), options);
            System.exit(-1);
            return;
        }

        if (cmdline.hasOption("help")) {
            Utils.printUsage(this.getClass().getName(), options);
            return;
        }


        String input = Utils.getStringOption(cmdline, "input");
        String output = Utils.getStringOption(cmdline, "output");
        String inputModel = Utils.getStringOption(cmdline, "inputModel");
        String hashFile = Utils.getStringOption(cmdline, "hash", "");
        boolean showName = Utils.getBooleanOption(cmdline, "showName", false);
        int max = Utils.getIntegerOption(cmdline, "max", 500);
        int n_thread = Utils.getIntegerOption(cmdline, "thread", 1);
        float threshold = Utils.getFloatOption(cmdline, "threshold", 0.005f);
        boolean onlyLeaf = Utils.getBooleanOption(cmdline, "onlyLeaf", false);
        boolean cami2 = Utils.getBooleanOption(cmdline, "cami2", false);
        boolean withoutUncult = Utils.getBooleanOption(cmdline, "withoutUncult", false);


        HSModel model = Classifier.makeModel(inputModel);
        if (hashFile.isEmpty()) {
            run_predict_hashed_reads(model, input, output, showName, max, threshold, onlyLeaf, withoutUncult);
        } else {
            run_predict_reads(hashFile, model, input, output, showName, max, threshold, onlyLeaf,
                    withoutUncult, n_thread, cami2);
        }
    }

    private Pair predict_one_read(String[] array, int kmerSize,
                                  HSModel model, boolean showName, int max, float threshold, boolean onlyLeaf, boolean withoutUncult, boolean cami2) throws Exception {
        String id = array[1];
        String seq = array[2];
        Tree tree = model.tree;
        stringVector kmers = sparc.generate_kmer_for_fastseq(seq, kmerSize, 'N', true);
        int[] words = new int[(int) kmers.size()];
        for (int i = 0; i < kmers.size(); i++) {
            int a = (int) sparc.lsh_hash_kmer(kmers.get(i), true);
            words[i] = a;
        }
        Record r = new Record(words);
        HSModel.PredPair[] ret = model.predict(r, max, threshold, onlyLeaf, withoutUncult);
        StringBuffer builder = new StringBuffer();

        for (int i = 0; i < ret.length; i++) {
            if (cami2) {
                String rank = tree.getRank(ret[i].label);
                if (!CAMI2_RANKS.contains(rank)) {
                    continue;
                }
            }
            if (!showName) {
                builder.append(ret[i].toString()).append(" ");
            } else {
                builder.append(String.format("%s:%.4f", tree.getName(ret[i].label), Math.exp(ret[i].score))).append(" ");
            }
        }
        String pred = builder.toString();
        return new Pair(id, pred);

    }

    private void run_predict_reads(String hashFile, HSModel model, String input, String output,
                                   boolean showName, int max, float threshold, boolean onlyLeaf,
                                   boolean withoutUncult, int n_thread, boolean cami2) throws Exception {
        MulticoreExecutor executor = null;
        if (n_thread > 1) {
            executor = new MulticoreExecutor(n_thread);
        }
        net.sparc.Info.load_native();
        sparc.lsh_load_from_file(hashFile);
        int kmerSize = sparc.lsh_get_kmer_size();
        Tree tree = model.tree;
        UniversalFileReader reader = null;
        UniversalFileWriter writer = null;
        List<PredTask> tasks = new ArrayList<>(BATCH_SIZE);

        try {
            reader = new UniversalFileReader(input);
            writer = new UniversalFileWriter(output);
            String line;
            while ((line = reader.readLine()) != null) {
                String[] array = line.split("\t");
                if (array.length != 3) {
                    System.out.println("Format error! Skip line: " + line);
                    continue;
                }
                if (executor == null) {
                    Pair ret = predict_one_read(array, kmerSize, model, showName, max, threshold, onlyLeaf, withoutUncult, cami2);
                    writer.writeLine((String) ret.getValue0() + "\t" + (String) ret.getValue1());
                } else {
                    PredTask predTask = new PredTask(array, kmerSize, model, showName, max, threshold, onlyLeaf, withoutUncult, cami2);
                    tasks.add(predTask);

                    if (tasks.size() >= BATCH_SIZE) {
                        for (Pair pari : executor.run(tasks)) {
                            writer.writeLine((String) pari.getValue0() + "\t" + (String) pari.getValue1());
                        }
                        tasks.clear();
                    }
                }

            }

            if (executor != null && tasks.size() > 0) {
                for (Pair pari : executor.run(tasks)) {
                    writer.writeLine((String) pari.getValue0() + "\t" + (String) pari.getValue1());
                }
                tasks.clear();
            }

        } finally {
            Objects.requireNonNull(writer).close();
            Objects.requireNonNull(reader).close();
            if (executor != null) {
                executor.shutdown();
            }
        }

    }

    public void run_predict_hashed_reads(HSModel model, String input, String output,
                                         boolean showName, int max, float threshold,
                                         boolean onlyLeaf, boolean withoutUncult) throws Exception {
        Tree tree = model.tree;
        UniversalFileReader reader = null;
        UniversalFileWriter writer = null;
        try {
            reader = new UniversalFileReader(input);
            writer = new UniversalFileWriter(output);
            String line;
            while ((line = reader.readLine()) != null) {
                Record r = new Record(line.split(" "));
                HSModel.PredPair[] ret = model.predict(r, max, threshold, onlyLeaf, withoutUncult);
                StringBuffer builder = new StringBuffer();
                if (false && showName) {
                    tree.getName(r.getLabel());
                } else {
                    builder.append(r.getLabel()).append(" ");
                }
                for (int i = 0; i < ret.length; i++) {
                    if (!showName) {
                        builder.append(ret[i].toString()).append(" ");
                    } else {
                        builder.append(String.format("%s:%.4f", tree.getName(ret[i].label), Math.exp(ret[i].score))).append(" ");
                    }
                }
                writer.writeLine(builder.toString());
            }
        } finally {
            Objects.requireNonNull(writer).close();
            Objects.requireNonNull(reader).close();
        }


    }

    class PredTask implements Callable<Pair> {


        private final String[] array;
        private final int kmerSize;
        private final HSModel model;
        private final boolean showName;
        private final int max;
        private final float threshold;
        private final boolean onlyLeaf;
        private final boolean withoutUncult;
        private final boolean cami2;

        public PredTask(String[] array, int kmerSize, HSModel model, boolean showName,
                        int max, float threshold, boolean onlyLeaf, boolean withoutUncult, boolean cami2) {
            this.array = array;
            this.kmerSize = kmerSize;
            this.model = model;
            this.showName = showName;
            this.max = max;
            this.threshold = threshold;
            this.onlyLeaf = onlyLeaf;
            this.withoutUncult = withoutUncult;
            this.cami2 = cami2;
        }

        @Override
        public Pair call() {
            try {
                return predict_one_read(array, kmerSize, model, showName, max, threshold, onlyLeaf, withoutUncult, cami2);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }


}
