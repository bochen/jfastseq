package net.jfastseq;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.zip.GZIPInputStream;


public class Tree implements Serializable {
    private static final long serialVersionUID = 5468164861548L;


    private final Map<Integer, TreeNode> nodeMap;
    private final int rootId;

    private transient Map<Integer, ArrayList<Boolean>> codes = new HashMap<>();
    private transient Map<Integer, ArrayList<Integer>> paths = new HashMap<>();
    private transient HashSet<Integer> uncultList = new HashSet<>();

    public Tree() {
        nodeMap = null;
        rootId = -1;
    }

    public Tree(String treePath) throws Exception {
        this(treePath, 1);
    }

    public Tree(String treePath, int rootId) throws Exception {
        this.rootId = rootId;
        this.nodeMap = readTree(treePath);
        this.build();
    }

    protected void build() throws Exception {
        this.valid();
        this.buildUncult();
        this.buildPaths();
        this.buildCodes();
    }


    private void valid() throws Exception {
        for (Map.Entry<Integer, TreeNode> entry : nodeMap.entrySet()) {
            TreeNode node = entry.getValue();
            if (node.nid != rootId) {
                if (node.parent < 1) {
                    throw new Exception(("parent <1 for node " + node));
                }
                if (!nodeMap.containsKey((node.parent))) {
                    throw new Exception(("parent not in list for  node " + node));
                }
            }
        }
    }

    protected ArrayList<Integer> getPath(int nid) {
        if (paths == null) {
            paths = new HashMap<>();
            paths.put(rootId, new ArrayList<>());
        }

        if (paths.containsKey(nid)) {
            return paths.get(nid);
        } else {
            TreeNode node = nodeMap.get(nid);
            int pid = node.getParent();
            ArrayList<Integer> p_paths = getPath(pid);
            ArrayList<Integer> this_paths = new ArrayList<>(p_paths);
            this_paths.add(pid);
            paths.put(nid, this_paths);
            return this_paths;
        }
    }

    protected ArrayList<String> getNamePath(int nid) {
        ArrayList<Integer> path = getPath(nid);
        ArrayList<String> ret = new ArrayList<>();
        for (int p : path) {
            ret.add(nodeMap.get(p).getName());
        }
        return ret;
    }

    protected ArrayList<String> getRankPath(int nid) {
        ArrayList<Integer> path = getPath(nid);
        ArrayList<String> ret = new ArrayList<>();
        for (int p : path) {
            ret.add(nodeMap.get(p).getRank());
        }
        return ret;
    }

    protected ArrayList<Boolean> getCode(int nid) {
        if (codes.containsKey(nid)) {
            return codes.get(nid);
        } else {
            TreeNode node = nodeMap.get(nid);
            int pid = node.getParent();
            TreeNode pnode = nodeMap.get(pid);
            boolean code;
            code = nid == pnode.left;
            ArrayList<Boolean> p_codes = getCode(pid);
            ArrayList<Boolean> this_codes = new ArrayList<>(p_codes);
            this_codes.add(code);
            codes.put(nid, this_codes);
            return this_codes;
        }
    }

    private void buildPaths() {
        if (paths == null) {
            paths = new HashMap<>();
        }
        paths.put(rootId, new ArrayList<>());
        for (Integer key : nodeMap.keySet()) {
            getPath(key);
        }
    }

    private void buildCodes() {
        if (codes == null) {
            codes = new HashMap<>();
        }

        codes.put(rootId, new ArrayList<>());
        for (Integer key : nodeMap.keySet()) {
            getCode(key);
        }
    }

    private void buildUncult() {
        if (uncultList == null) {
            uncultList = new HashSet<>();
        }
        for (TreeNode node : nodeMap.values()) {
            if (node.name.toLowerCase().contains("uncult")) {
                uncultList.add(node.nid);
            }
        }
        System.out.println("Tree has " + uncultList.size() + " uncultured taxIds");
    }


    public Map<Integer, ArrayList<Integer>> getPaths() {
        return paths;
    }

    public Map<Integer, ArrayList<Boolean>> getCodes() {
        return codes;
    }

    public static Map<Integer, TreeNode> readTree(String treepath) throws IOException {
        //create ObjectMapper instance
        FileInputStream fin = new FileInputStream(treepath);
        GZIPInputStream gzis = new GZIPInputStream(fin);
        InputStreamReader reader = new InputStreamReader(gzis);
        BufferedReader in = new BufferedReader(reader);

        ObjectMapper objectMapper = new ObjectMapper();
        Map<Integer, TreeNode> myMap = objectMapper.readValue(in,
                new TypeReference<Map<Integer, TreeNode>>() {
                });
        return myMap;
    }

    public int size() {
        return nodeMap.size();
    }

    public boolean hasNode(int node){
        return nodeMap.containsKey(node);
    }
    
    public TreeNode getNode(int node) {
        return nodeMap.get(node);
    }

    public int getRootId() {
        return rootId;
    }

    public String getName(int nid) {
        return nodeMap.get(nid).name;
    }

    public boolean isUncult(int nid) {
        return this.uncultList.contains(nid);
    }

    public String getRank(int nid) {
        return nodeMap.get(nid).rank;
    }
}
