package net.jfastseq;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.github.luben.zstd.ZstdInputStream;
import com.github.luben.zstd.ZstdOutputStream;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class ContClassifier {

    private final static org.slf4j.Logger logger = LoggerFactory.getLogger(ContClassifier.class);
    protected Long queueSize = 0L;

    private boolean trainable = true;
    protected WordCounter word_counts;
    private Args args;
    private LookupTable input_weights;
    private LookupTable output_weights;
    private Tree tree;
    private int current_epoch = 0;
    protected boolean thread_running = false;

    private final static Kryo kryo = new Kryo();

    static {
        kryo.register(String[].class);
        kryo.register(float[].class);
        kryo.register(HashMap.class);
        kryo.register(java.util.LinkedHashMap.class);
        kryo.register(ConcurrentHashMap.class);

        kryo.register(SaverObj.class);
        kryo.register(Args.class);
        kryo.register(LookupTable.class);
        kryo.register(WordCounter.class);
        kryo.register(Tree.class);
        kryo.register(TreeNode.class);
    }

    private AdminRunnable adminThreadRunnable;
    private ArrayList<Command> commandQueue = new ArrayList<>();
    private ContTestRunnable testThread;
    private long num_total_processed_reads = 0;

    public static HSModel makeModel(String modelFile) throws Exception {
        ContClassifier clf = new ContClassifier(modelFile);
        HSModel model = new HSModel(clf.args.getDim(), clf.getInputWeights(), clf.getOutputWeights(), clf.getTree(), null);
        return model;

    }

    public ContClassifier(String aModelFile) throws Exception {
        this.trainable = false;
        this.load(aModelFile);
        this.tree.build();
    }

    public ContClassifier(Args args) throws Exception {
        String resume = args.getResumeModel();
        if (!resume.isEmpty()) {
            this.load(resume);
            this.tree.build();
            if (args.get("thread") != null) {
                this.args.put("thread", args.getThread());
            }
            if (args.get("adminPort") != null) {
                this.args.put("adminPort", args.getAdminPort());
            }

            if (args.get("port") != null) {
                this.args.put("port", args.getRedisPort());
            }

            if (args.get("address") != null) {
                this.args.put("address", args.getRedisHost());
            }

            if (args.get("epoch") != null) {
                this.args.put("epoch", args.get("epoch"));
            }

            if (args.get("lr") != null) {
                this.args.put("lr", args.get("lr"));
            }

            if (args.get("tree") != null) {
                this.args.put("tree", args.get("tree"));
            }

            this.args.forEach((k, v) -> {
                logger.info(String.format("%s %s=%s", (v == null ? "null" : v.getClass().getName()), k, (v == null ? "null" : v.toString())));
            });
            checkArgs(true);
        } else {
            this.args = args;
            checkArgs(false);

            String output = (String) args.get("output_folder");
            Utils.create_folder(output);
            int dim = args.getDim();
            word_counts = new WordCounter(args.getEstimatedWordSize());
            input_weights = new LookupTable(dim, args.getEstimatedWordSize());
            this.output_weights = new LookupTable(dim, args.getEstimatedLabelSize());
            String tree_file = (String) args.get("tree");
            if (true) {
                logger.info("Building tree ...");
                tree = new Tree(tree_file);
                logger.info("Tree length: " + tree.size());
                logger.info("Finish building tree ");
            }
        }
    }

    public static Integer[] percentile(Collection<Integer> values, double[] percentiles) {
        List<Integer> list = new ArrayList(values);
        Collections.sort(list);
        Integer[] ret = new Integer[percentiles.length];
        for (int i = 0; i < percentiles.length; i++) {
            int index = (int) Math.ceil((percentiles[i] / 100) * list.size());
            ret[i] = list.get(index - 1);
        }

        return ret;
    }

    private String getInfo() {
        return getInfo(true);
    }

    private String getInfo(boolean with_percentile) {
        StringBuilder buffer = new StringBuilder();
        buffer.append(String.format("WordCounter  Size: %d", word_counts.size()));
        buffer.append('\n');
        buffer.append(String.format("InputWeight  Dim : %s", input_weights.shape()));
        buffer.append('\n');
        buffer.append(String.format("OutputWeight Dim : %s", output_weights.shape()));
        buffer.append('\n');
        buffer.append(String.format("enableWordCount : %s\n", String.valueOf(args.getEnableWordCount())));
        buffer.append(String.format("word_counts.allowInsert : %s\n", String.valueOf(word_counts.getAllowInsert())));
        buffer.append(String.format("input_weights.allowInsert : %s\n", String.valueOf(input_weights.getAllowInsert())));
        buffer.append(String.format("output_weights.allowInsert : %s\n", String.valueOf(output_weights.getAllowInsert())));
        buffer.append(String.format("queueSize : %s\n", String.valueOf(queueSize)));
        buffer.append(String.format("#processed reads : %s\n", String.valueOf(num_total_processed_reads)));
        buffer.append(String.format("queueSize : %s\n", String.valueOf(thread_running)));

        if (with_percentile) {
            if (word_counts.size() > 0) {
                Collection<Integer> wc = word_counts.values();
                double[] a = new double[]{1, 2, 3, 4, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 95, 98};
                Integer[] percentiles = percentile(wc, a);
                for (int i = 0; i < a.length; i++) {
                    buffer.append(String.format("percentile %d: %d", (int) a[i], percentiles[i]));
                    buffer.append('\n');
                }
            }
        }
        buffer.append("#of pending commands: " + commandQueue.size() + "\n");
        for (Command cmd : commandQueue) {
            buffer.append("command " + cmd.getClass().getName() + " : " + cmd + "\n");
        }
        return buffer.toString();
    }

    private void print_info() {
        String info = getInfo();
        String[] lines = info.split("\n");
        for (int i = 0; i < lines.length; i++) {
            logger.info(lines[i]);
        }
    }

    private void checkArgs(boolean resume) throws Exception {
        String output = args.getOutputFolder();
        if (!resume && Utils.path_exists(output, true)) {
            throw new Exception("output fold exists: " + output);
        }

        if (!resume) {
            String tree_file = (String) args.get("tree");
            if (!Utils.path_exists(tree_file, false)) {
                throw new Exception("no tree file found:  " + tree_file);
            }
        }
    }

    public void train0() {
        start_admin_thread();
    }

    public void train() throws Exception {
        normal_train();
    }


    public void normal_train() throws Exception {
        if (!this.trainable) {
            throw new RuntimeException("model is not trainable");
        }
        if (current_epoch == 0) {
            save_model(-1);
            print_info();
        }
        int n_epoch = args.getEpoch();
        start_admin_thread();
        start_test_thread();

        for (int i = current_epoch; i < n_epoch; i++) {
            logger.info("start epoch " + i);
            thread_running = true;

            start_threads(i);
            thread_running = false;

            logger.info("checkpoint epoch " + i);
            current_epoch = i + 1;
            print_info();
            if (!commandQueue.isEmpty()) {
                for (Command command : commandQueue) {
                    command.run(this);
                }
                commandQueue.clear();
                logger.info("After running queued command");
                print_info();

            }
            if (i + 1 == n_epoch || i % args.getSavePeriod() == 0) {
                save_model(i);
            }
        }


        for (int i = 0; i < 60; i++) {
            this.testThread.setStop(true);
            this.adminThreadRunnable.setStop(true);
            testThread.join(1000);
            adminThreadRunnable.join(4000);
        }
        adminThreadRunnable.interrupt();
        testThread.interrupt();
    }

    private void load(String modelpath) throws IOException {
        // Reading the object from a file
        logger.info("read model from " + modelpath);
        FileInputStream file = new FileInputStream(modelpath);
        ZstdInputStream zis = new ZstdInputStream(file);

        Input input = new Input(zis);
        SaverObj objects = kryo.readObject(input, SaverObj.class);
        input.close();
        zis.close();
        file.close();

        this.current_epoch = objects.current_epoch;
        this.num_total_processed_reads = objects.num_total_processed_reads;
        this.args = objects.args;
        this.tree = objects.tree;
        this.word_counts = objects.word_counts;
        this.input_weights = objects.input_weights;
        this.output_weights = objects.output_weights;

        logger.info("finish reading model from  " + modelpath);
    }

    private void save_model(int epoch) throws Exception {
        String output = (String) args.get("output_folder");
        String output_file = Paths.get(output, "tmp_model_" + epoch).toAbsolutePath().toString();
        logger.info("writing model to " + output_file);
        //Saving of object in a file
        FileOutputStream file = new FileOutputStream(output_file);
        ZstdOutputStream zos = new ZstdOutputStream(file);
        Output okyro = new Output(zos);

        // Method for serialization of object
        SaverObj objects = new SaverObj();
        int i = 0;
        objects.current_epoch = (this.current_epoch);
        objects.num_total_processed_reads = this.num_total_processed_reads;
        objects.args = (this.args);
        objects.tree = (this.tree);
        objects.word_counts = (this.word_counts);
        objects.input_weights = (this.input_weights);
        objects.output_weights = (this.output_weights);
        kryo.writeObject(okyro, objects);

        okyro.close();
        zos.close();
        file.close();
        logger.info("finish writing " + output_file);
        String true_output_file = Paths.get(output, "model_" + epoch).toAbsolutePath().toString();
        File src_file = new File(output_file);
        File dest_file = new File(true_output_file);
        src_file.renameTo(dest_file);
        logger.info("rename " + src_file.getAbsolutePath() + " to " + dest_file.getAbsolutePath());

    }

    public LookupTable getInputWeights() {
        return input_weights;
    }

    public LookupTable getOutputWeights() {
        return output_weights;
    }

    private void start_threads(int epoch) throws InterruptedException {
        int n_thread = args.getThread();


        int n_epoch = args.getEpoch();
        float lr = args.getLearningRate();
        double lr_start;
        if (args.is_rcc()) {
            long n = args.getEpochSize() * n_epoch;
            lr_start = (1.0 - 1.0 * num_total_processed_reads / n) * lr;
        } else {
            lr_start = (1.0 - 1.0 * epoch / n_epoch) * lr;
        }
        logger.info(String.format("epoch %d, learning rate: %f", epoch, lr_start));
        ArrayList<ContTrainRunnable> threads = new ArrayList<>();
        int epochSize = args.getEpochSize() / n_thread;
        for (int i = 0; i < n_thread; i++) {
            ContTrainRunnable thread = new ContTrainRunnable(i, this, lr_start, epochSize, args.getEnableWordCount());
            threads.add(thread);
            thread.start();
        }


        for (int i = 0; i < threads.size(); i++) {
            threads.get(i).join();
        }
        for (int i = 0; i < threads.size(); i++) {
            num_total_processed_reads += threads.get(i).num_total_processed_reads;
        }

    }

    private void start_admin_thread() {
        this.adminThreadRunnable = new AdminRunnable(this);
        this.adminThreadRunnable.start();
    }

    private void start_test_thread() {
        this.testThread = new ContTestRunnable(this);
        this.testThread.start();
    }


    public Tree getTree() {
        return this.tree;
    }

    public Args getArgs() {
        return args;
    }

    static class AdminRunnable extends Thread {

        private final ContClassifier classifier;
        private int port;
        private boolean goingStop = false;
        private ServerSocketChannel channel = null;

        public AdminRunnable(ContClassifier contClassifier) {
            this.classifier = contClassifier;
            this.port = classifier.args.getAdminPort();
        }

        public void setStop(boolean b) {
            this.goingStop = b;
        }

        private ServerSocketChannel createServerSocketChannel() throws IOException {
            ServerSocketChannel serverSocket = null;
            serverSocket = ServerSocketChannel.open();
            serverSocket.socket().bind(new InetSocketAddress(port));
            serverSocket.configureBlocking(false);
            this.channel = serverSocket;
            System.err.println("Server started at port " + port);
            return channel;
        }

        @Override
        public void run() {
            try {
                channel = this.createServerSocketChannel();

                while (!goingStop) {
                    SocketChannel client =
                            channel.accept();
                    if (client == null) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } else {
                        readFrom(client);
                    }
                }

                if (channel != null) channel.close();

            } catch (IOException i) {
                System.out.println(i);
            }


        }

        private void process(SocketChannel client, String msg) throws IOException {

            System.err.println("received: `" + msg + "`");
            String[] words = msg.trim().split(" ");
            String reply = "";
            String command;
            if (words.length == 0) {
                reply = "Error, empty message.";
            } else {
                command = words[0];
                if (command.equals("info")) {
                    reply = classifier.getInfo(false);
                } else if (command.equals("args")) {
                    for (String k : classifier.args.keySet()) {
                        reply += String.format("%s=%s\n", k, classifier.args.get(k).toString());
                    }
                    reply += String.format("%s=%d\n", "current_epoch", classifier.current_epoch);
                    double current_lr = (1.0 - 1.0 * classifier.current_epoch / classifier.args.getEpoch()) * classifier.args.getLearningRate();
                    reply += String.format("%s=%f\n", "current_lr", current_lr);
                } else if (command.equals("clearCommandQueue")) {
                    classifier.commandQueue.clear();
                    reply = "OK";
                } else if (command.equals("enableWordCount")) {
                    classifier.args.setEnableWordCount(true);
                    reply = "Enabled word counting, however it will effect for next epoch.";
                } else if (command.equals("disableWordCount")) {
                    classifier.args.setEnableWordCount(false);
                    reply = "Disabled word counting, however it will effect for next epoch.";
                } else if (command.equals("enableNewWord")) {
                    classifier.input_weights.setAllowInsert(true);
                    classifier.word_counts.setAllowInsert(true);
                    reply = "OK";
                } else if (command.equals("disableNewWord")) {
                    classifier.input_weights.setAllowInsert(false);
                    classifier.word_counts.setAllowInsert(false);
                    reply = "OK";
                } else if (command.equals("pruneWord")) {
                    if (words.length == 2) {
                        Integer val = Integer.valueOf(words[1]);
                        Command cmd = new PruneWord(val);
                        classifier.commandQueue.add(cmd);
                        reply = "pruneWord is in the queue, will run before next epoch";
                    } else {
                        reply = "Usage: pruneWord <num>";
                    }
                } else if (command.equals("set")) {
                    if (words.length != 3) {
                        reply = "Error! set <key> <val>";
                    } else {
                        Args args = classifier.args;
                        String key = words[1];
                        String val = words[2];
                        if (args.containsKey(key)) {
                            reply = args.set(key, val);
                        } else if (key.equals("current_epoch")) {
                            int oldval = classifier.current_epoch;
                            classifier.current_epoch = Integer.valueOf(val);
                            reply = String.format("set %s from %d to %d ", key, oldval, classifier.current_epoch);

                        } else if (key.equals("current_lr")) {
                            Float lr = Float.valueOf(val) / (1.0f - 1.0f * classifier.current_epoch / classifier.args.getEpoch());
                            reply = args.set("lr", lr.toString());
                        } else {
                            reply = "unknown key " + key;
                        }
                    }
                } else {
                    reply = "Error, has not implemented yet for `" + command + "`";
                }
            }
            if (reply == null || reply.trim().isEmpty()) {
                reply = "something is wrong. have bugs.";
            }
            writeTo(client, reply);
        }

        private void writeTo(SocketChannel client, String message) throws IOException {
            ByteBuffer buf = ByteBuffer.allocate(4 * 1024);
            byte[] bytes = message.getBytes();
            for (int i = 0; i < bytes.length; i++)
                buf.put(bytes[i]);
            buf.put((byte) '\n');
            buf.put((byte) '\n');
            System.out.println(buf.toString());
            buf.flip();
            int bytesWrite = 0;
            while (bytesWrite < bytes.length) {
                int a = client.write(buf);
                System.out.println("Write " + a + "  chars ");
                bytesWrite += a;
            }

            System.out.println("Write: " + message);
        }

        private void readFrom(SocketChannel client) throws IOException {
            ByteBuffer buf = ByteBuffer.allocate(4 * 1024);
            StringBuilder builder = new StringBuilder();
            int bytesRead = client.read(buf); //read into buffer.
            while (bytesRead != -1) {
                buf.flip();  //make buffer ready for read
                while (buf.hasRemaining()) {
                    char c = (char) buf.get();
                    if (c == '\n') {
                        try {
                            process(client, builder.toString());
                            //client.finishConnect();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return;
                    } else {
                        builder.append(c);
                    }

                }

                buf.clear(); //make buffer ready for writing
                bytesRead = client.read(buf);
            }
        }


    }

    static abstract class Command {
        abstract void run(ContClassifier clf);
    }

    static class PruneWord extends Command {
        private final Integer minword;

        public PruneWord(Integer minword) {
            super();
            this.minword = minword;
        }

        @Override
        public String toString() {
            return "PruneWord{" +
                    "minword=" + minword +
                    '}';
        }

        @Override
        void run(ContClassifier clf) {
            WordCounter wc = clf.word_counts;
            LookupTable lt = clf.input_weights;
            ArrayList<Integer> words = new ArrayList<>();
            for (Integer word : wc.getTable().keySet()) {
                if (wc.get(word) < minword) {
                    wc.remove(word);
                    lt.remove(word);
                }
            }
        }
    }

}
