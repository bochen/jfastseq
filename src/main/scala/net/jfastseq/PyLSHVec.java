package net.jfastseq;

import net.sparc.sparc;
import net.sparc.stringVector;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Callable;


public class PyLSHVec {
    private static final HashSet<String> CAMI2_RANKS = new HashSet<>();

    static {
        CAMI2_RANKS.add("superkingdom");
        CAMI2_RANKS.add("phylum");
        CAMI2_RANKS.add("class");
        CAMI2_RANKS.add("order");
        CAMI2_RANKS.add("family");
        CAMI2_RANKS.add("genus");
        CAMI2_RANKS.add("species");
    }

    private String modelFile;
    private String hashFile;
    private int batchsize = 1024 * 8;

    private int maxItems = 500;
    private boolean onlyShowLeaf = false;
    private int numThread = 1;
    private float threshold = 0.005f;
    private boolean withoutUncult = false;
    private boolean onlyShowMainTax = true;
    private int kmerSize = -1;
    private HSModel model = null;
    private boolean hasInitialized = false;
    MulticoreExecutor executor = null;
    private LookupTable inputMap;
    private Tree tree;

    public PyLSHVec(String modelFile, String hashFile) throws Exception {
        this.modelFile = modelFile;
        this.hashFile = hashFile;
        if (!Utils.path_exists(hashFile, false)) {
            throw new Exception("Hash file does not exit: " + hashFile);
        }
        if (!Utils.path_exists(modelFile, false)) {
            throw new Exception("Model file does not exit: " + modelFile);
        }
    }

    public void initialize() throws Exception {
        net.sparc.Info.load_native();
        sparc.lsh_load_from_file(hashFile);
        this.kmerSize = sparc.lsh_get_kmer_size();
        this.model = ContClassifier.makeModel(modelFile);
        if (kmerSize < 1) {
            throw new Exception("kmer size is negative.");
        }
        if (model == null) {
            throw new Exception("make model failed.");
        }
        if (numThread > 1) {
            executor = new MulticoreExecutor(numThread);
        }

        this.inputMap = model.wi_;

        this.tree = model.tree;


        this.hasInitialized = true;
    }

    public boolean hasNode(int nid) {
        return this.tree.hasNode(nid);
    }

    public String getRank(int nid) {
        if (hasNode(nid)) {
            return this.tree.getRank(nid);
        } else {
            return null;
        }

    }

    public String getName(int nid) {
        if (hasNode(nid)) {
            return this.tree.getName(nid);
        } else {
            return null;
        }

    }


    public Integer[] getTaxIdPath(int nid) {
        if (hasNode(nid)) {
            ArrayList<Integer> v = this.tree.getPath(nid);
            return v.toArray(new Integer[0]);
        } else {
            return null;
        }

    }

    public String[] getTaxNamePath(int nid) {

        if (hasNode(nid)) {
            ArrayList<String> v = this.tree.getNamePath(nid);
            return v.toArray(new String[0]);
        } else {
            return null;
        }

    }


    public float[] getEmbedding(String seq) {
        stringVector kmers = sparc.generate_kmer_for_fastseq(seq, kmerSize, 'N', true);
        int[] words = new int[(int) kmers.size()];
        for (int i = 0; i < kmers.size(); i++) {
            int a = (int) sparc.lsh_hash_kmer(kmers.get(i), true);
            words[i] = a;
        }
        Record r = new Record(words);

        Vector vec = new Vector(this.inputMap.dim);
        vec.zero();
        for (int word : r.getWords()) {
            if (inputMap.contains(word)) {
                vec.addRow(inputMap, word);
            }

        }
        if (vec.size() > 0) {
            vec.divide(vec.size());
        }

        float[] embed = new float[vec.size()];
        for (int i = 0; i < embed.length; i++) {
            embed[i] = vec.get(i);
        }
        return embed;
    }

    public float[][] getEmbedding(String[] reads) throws Exception {
        List<EmbeddingTask> tasks = new ArrayList<>(batchsize);
        List<float[]> predicts = new ArrayList<>();
        try {
            for (String read : reads) {
                if (executor == null) {
                    float[] ret = getEmbedding(read);
                    predicts.add(ret);
                } else {
                    EmbeddingTask predTask = new EmbeddingTask(read);
                    tasks.add(predTask);

                    if (tasks.size() >= batchsize) {
                        for (float[] p : executor.run(tasks)) {
                            predicts.add(p);
                        }
                        tasks.clear();
                    }
                }
            }
            if (executor != null && tasks.size() > 0) {
                for (float[] p : executor.run(tasks)) {
                    predicts.add(p);
                }
                tasks.clear();
            }
        } finally {
            if (false && executor != null) {
                executor.shutdown();
            }
        }
        return predicts.toArray(new float[0][0]);
    }

    public String predict(String seq) throws Exception {
        stringVector kmers = sparc.generate_kmer_for_fastseq(seq, kmerSize, 'N', true);
        int[] words = new int[(int) kmers.size()];
        for (int i = 0; i < kmers.size(); i++) {
            int a = (int) sparc.lsh_hash_kmer(kmers.get(i), true);
            words[i] = a;
        }
        Record r = new Record(words);
        HSModel.PredPair[] ret = model.predict(r, maxItems, threshold, onlyShowLeaf, withoutUncult);
        StringBuffer builder = new StringBuffer();

        for (int i = 0; i < ret.length; i++) {
            if (onlyShowMainTax) {
                String rank = tree.getRank(ret[i].label);
                if (!CAMI2_RANKS.contains(rank)) {
                    continue;
                }
            }

            builder.append(ret[i].toString()).append(" ");

        }
        String pred = builder.toString();
        return pred;

    }

    public String[] predict(String[] reads) throws Exception {
        List<PredTask> tasks = new ArrayList<>(batchsize);
        List<String> predicts = new ArrayList<>();
        try {
            for (String read : reads) {
                if (executor == null) {
                    String ret = predict(read);
                    predicts.add(ret);
                } else {
                    PredTask predTask = new PredTask(read);
                    tasks.add(predTask);

                    if (tasks.size() >= batchsize) {
                        for (String p : executor.run(tasks)) {
                            predicts.add(p);
                        }
                        tasks.clear();
                    }
                }
            }
            if (executor != null && tasks.size() > 0) {
                for (String p : executor.run(tasks)) {
                    predicts.add(p);
                }
                tasks.clear();
            }
        } finally {
            if (false && executor != null) {
                executor.shutdown();
            }
        }
        return predicts.toArray(new String[0]);
    }


    class PredTask implements Callable<String> {


        private final String read;


        public PredTask(String read) {
            this.read = read;
        }

        @Override
        public String call() {
            try {
                return predict(read);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }


    class EmbeddingTask implements Callable<float[]> {


        private final String read;

        public EmbeddingTask(String read) {
            this.read = read;
        }

        @Override
        public float[] call() {
            try {
                return getEmbedding(read);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public int getBatchsize() {
        return batchsize;
    }

    public int getVecDim() {
        return this.inputMap.dim;
    }

    public void setBatchsize(int batchsize) {
        this.batchsize = batchsize;
    }

    public int getMaxItems() {
        return maxItems;
    }

    public void setMaxItems(int maxItems) {
        this.maxItems = maxItems;
    }

    public boolean isOnlyShowLeaf() {
        return onlyShowLeaf;
    }

    public void setOnlyShowLeaf(boolean onlyShowLeaf) {
        this.onlyShowLeaf = onlyShowLeaf;
    }

    public int getNumThread() {
        return numThread;
    }

    public void setNumThread(int numThread) throws Exception {
        if (this.hasInitialized) {
            throw new Exception("pls set #thread before initializing");
        }
        this.numThread = numThread;
    }

    public float getThreshold() {
        return threshold;
    }

    public void setThreshold(float threshold) {
        this.threshold = threshold;
    }

    public boolean isWithoutUncult() {
        return withoutUncult;
    }

    public void setWithoutUncult(boolean withoutUncult) {
        this.withoutUncult = withoutUncult;
    }

    public boolean isOnlyShowMainTax() {
        return onlyShowMainTax;
    }

    public void setOnlyShowMainTax(boolean onlyShowMainTax) {
        this.onlyShowMainTax = onlyShowMainTax;
    }

    public int getKmerSize() {
        return kmerSize;
    }

    public boolean isInitialized() {
        return hasInitialized;
    }

}