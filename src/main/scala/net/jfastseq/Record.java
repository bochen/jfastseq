package net.jfastseq;

import java.util.ArrayList;
import java.util.List;

public class Record {

    private static final String LABEL_TAG = "__label__";
    private static final int LABEL_LENGTH = LABEL_TAG.length();
    private final Integer label;
    private int[] words;

    public Record(int[] words) {
        this.words = words;
        label = -1;

    }

    public Record(String[] words) {
        assert (words.length > 1);
        String label = words[0];
        if (label.toLowerCase().startsWith(LABEL_TAG)) {
            label = label.substring(LABEL_LENGTH);
        }
        this.label = Integer.valueOf(label);
        this.words = new int[words.length - 1];
        for (int i = 1; i < words.length; i++) {
            this.words[i - 1] = (int) Utils.base64toulong(words[i]);
        }
    }

    public int[] getWords() {
        return words;
    }


    public int getWord(int i) {
        return words[i];
    }

    public Integer getLabel() {
        return label;
    }

    public int size() {
        return words.length;
    }
}
