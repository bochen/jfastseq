package net.jfastseq;


import java.io.Serializable;
import java.util.Objects;

public class TreeNode implements Serializable {
    private static final long serialVersionUID = 362435841265L;

    int count;
    boolean binary;
    int right;
    int left;
    String name;
    int parent;
    String rank;
    int nid;

    public boolean isDummy() {
        return "dummy".equals(rank);
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isBinary() {
        return binary;
    }

    public void setBinary(boolean binary) {
        this.binary = binary;
    }

    public int getRight() {
        return right;
    }

    public void setRight(int right) {
        this.right = right;
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getParent() {
        return parent;
    }

    public void setParent(int parent) {
        this.parent = parent;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public int getNid() {
        return nid;
    }

    public void setNid(int nid) {
        this.nid = nid;
    }

    @Override
    public String toString() {
        return "TreeNode{" +
                "count=" + count +
                ", binary=" + binary +
                ", right=" + right +
                ", left=" + left +
                ", name='" + name + '\'' +
                ", parent=" + parent +
                ", rank='" + rank + '\'' +
                ", nid=" + nid +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TreeNode treeNode = (TreeNode) o;
        return count == treeNode.count &&
                binary == treeNode.binary &&
                right == treeNode.right &&
                left == treeNode.left &&
                parent == treeNode.parent &&
                nid == treeNode.nid &&
                Objects.equals(name, treeNode.name) &&
                Objects.equals(rank, treeNode.rank);
    }

    @Override
    public int hashCode() {
        return Objects.hash(count, binary, right, left, name, parent, rank, nid);
    }

    public boolean isLeaf() {
        return left < 0 && right < 0;
    }
}
