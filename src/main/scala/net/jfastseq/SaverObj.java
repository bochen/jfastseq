package net.jfastseq;

import java.io.Serializable;

public class SaverObj implements Serializable {
    private static final long serialVersionUID = 46686248L;

    public int current_epoch;
    public long num_total_processed_reads;
    public Args args;
    public Tree tree;
    public WordCounter word_counts;
    public LookupTable input_weights;
    public LookupTable output_weights;
}