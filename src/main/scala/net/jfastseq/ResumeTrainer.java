package net.jfastseq;

import org.apache.commons.cli.*;

import java.util.logging.Logger;

public class ResumeTrainer {
    private final static Logger logger = MyFormatter.getLogger(ResumeTrainer.class.getName());

    public void run(String[] argv) throws Exception {

        Args args = parse_args(argv);
        args.forEach((k, v) -> {
            logger.info(String.format("%s %s=%s", (v == null ? "null" : v.getClass().getName()), k, (v == null ? "null" : v.toString())));
        });
        new Classifier(args).train();
    }


    protected Args parse_args(String[] argv) throws Exception {
        Option help = new Option("help", "print this message");

        Option debug = new Option("debug", "print debugging information");

        Options options = new Options();
        Utils.create_option("thread", "number of threads", Number.class, options);
        Utils.create_option("inputModel", "input model to resume training", String.class, options);

        Option input_files = Option.builder("i").argName("input")
                .hasArgs().longOpt("input")
                .type(String[].class)
                .desc("input data files")
                .build();


        options.addOption(help);
        options.addOption(debug);
        options.addOption(input_files);
        CommandLineParser parser = new DefaultParser();
        CommandLine line = parser.parse(options, argv);

        Integer thread = null;
        if (line.hasOption("thread")) {
            thread = Integer.valueOf(line.getOptionValue("thread"));
        }


        String inputModel;
        if (line.hasOption("inputModel")) {
            inputModel = line.getOptionValue("inputModel");
        } else {
            throw new Exception("input model has to be specified.");
        }

        Args args = new Args();
        args.put("thread", thread);
        args.put("inputModel", inputModel);
        if (line.hasOption("input")) {
            String[] patterns = line.getOptionValues("input");
            String[] files = Utils.findFiles(patterns);
            args.put("input_files", files);
        }
        return args;
    }

}
