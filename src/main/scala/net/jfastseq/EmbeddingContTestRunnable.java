package net.jfastseq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Response;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

public class EmbeddingContTestRunnable extends Thread {

    private final EmbeddingCont classifier;
    private final EmbeddingModel model;
    private final Args args;
    private Logger logger;
    private boolean going_stop = false;

    public EmbeddingContTestRunnable(EmbeddingCont classifier) {
        this.classifier = classifier;
        logger = LoggerFactory.getLogger(EmbeddingContTestRunnable.class);

        int dim = classifier.getArgs().getDim();
        this.args = classifier.getArgs();
        long echo_per_n_read = args.getEchoPeriod();
        this.model = new EmbeddingModel
                (dim, classifier.getInputWeights(),
                        classifier.getOutputWeights(), args.getNegSize(), classifier.word_counts, classifier.getNegatives(), args.getHalfWindow(), args.useCbow());


    }

    public void run() {
        String host = args.getRedisHost();
        int port = args.getRedisPort();
        String queue = args.getQueueName();
        byte[] queueBytes = queue.getBytes();
        byte[] testQueueBytes = (queue + ".test").getBytes();
        logger.info(String.format("connect to %s:%d, use queue: %s", host, port, queue));
        Jedis jedis = new Jedis(host, port);
        ArrayList<Float> loss_history = new ArrayList<>();
        long total_num = 0;
        while (!going_stop) {
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(!classifier.thread_running){
                continue;
            }
            Pipeline p = jedis.pipelined();
            Response<Set<byte[]>> rep1 = p.spop(testQueueBytes, 10);
            Response<Long> rep2 = p.scard(queueBytes);
            p.sync();
            classifier.queueSize = rep2.get();
            Set<byte[]> ret = rep1.get();
            ReadMsgProtos.Read read = null;

            if (ret.size() < 5) {
                try {
                    logger.info("Starving...");
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                float loss = 0f;
                int loss_count = 0;
                for (byte[] bytes : ret) {
                    try {
                        read = ReadMsgProtos.Read.parseFrom(bytes);
                        if(read.getKmerCount()>0) {
                            loss += model.calcLoss(read);
                            loss_count++;
                        }

                    } catch (Exception e) {
                        System.err.println(Arrays.toString(bytes));
                        if (read != null) {
                            System.err.println(read.toString());
                        }
                        e.printStackTrace();
                    }

                }
                total_num++;
                if(loss_count>0) {
                    float meanloss = loss / loss_count;
                    loss_history.add(meanloss);
                    System.err.println(String.format("Test_loss:\t%d\t%.3f", System.currentTimeMillis(), meanloss));
                }
                if (total_num > 0 && total_num % 60 == 0) {
                    float mean_minutes = mean_loss(loss_history, 60);
                    float mean_15_minutes = mean_loss(loss_history, 60 * 15);
                    float mean_30_minutes = mean_loss(loss_history, 60 * 30);
                    float mean_60_minutes = mean_loss(loss_history, 60 * 60);
                    float mean_120_minutes = mean_loss(loss_history, 60 * 120);
                    float mean_600_minutes = mean_loss(loss_history, 60 * 600);
                    System.err.println(String.format("History loss: min %.3f, 15min %.3f, 30min %.3f, 1hr %.3f, 2hr %.3f, 5hr %.3f",
                            mean_minutes, mean_15_minutes, mean_30_minutes, mean_60_minutes, mean_120_minutes, mean_600_minutes));
                }
            }


        }
    }

    private float mean_loss(ArrayList<Float> loss_history, int last_n) {
        if (loss_history.size() < last_n) {
            return -1f;
        } else {
            float loss = 0f;
            for (int i = loss_history.size() - last_n; i < loss_history.size(); i++) {
                loss += loss_history.get(i);
            }
            return loss / last_n;

        }
    }

    public void setStop(boolean b) {
        this.going_stop = b;
    }
}