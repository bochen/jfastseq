package net.jfastseq;

import org.apache.commons.cli.*;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;

public class EmbeddingContTrainer {
    private final static org.slf4j.Logger logger = LoggerFactory.getLogger(EmbeddingContTrainer.class);

    public void run(String[] argv) throws Exception {

        Args args = parse_args(argv);
        if (args == null) return;
        args.forEach((k, v) -> {
            logger.info(String.format("%s %s=%s", (v == null ? "null" : v.getClass().getName()), k, (v == null ? "null" : v.toString())));
        });


        new EmbeddingCont(args).train();
    }


    protected Args parse_args(String[] argv) throws Exception {
        Option help = new Option("h", "help", false, "print this message");

        Options options = new Options();
        Utils.create_option("lr", "learning rate", Number.class, options);
        Utils.create_option("dim", "word dimension", Number.class, options);
        Utils.create_option("halfWindow", "half window size", Number.class, options);
        Utils.create_option("negSize", "# of negative samples", Number.class, options);
        Utils.create_option("embeddingType", "cbow or skipgram", String.class, options, true);
        Utils.create_option("epoch", "training epoch", Number.class, options);
        Utils.create_option("epochSize", "#of reads per epoch", String.class, options, true);
        Utils.create_option("thread", "number of threads", Number.class, options);
        Utils.create_option("estimatedWordSize", "estimatedWordSize", Number.class, options);
        Utils.create_option("savePeriod", "savePeriod (per #epoch)", Number.class, options);
        Utils.create_option("echoPeriod", "echoPeriod (per #reads)", Number.class, options);
        Utils.create_option("address", "redis host", String.class, options);
        Utils.create_option("queue", "queue name", String.class, options);
        Utils.create_option("port", "redis port", Number.class, options);
        Utils.create_option("adminPort", "admin port", Number.class, options);
        Utils.create_option("minQueueSize", "min queue size to start train", Number.class, options);


        Option output_folder = Option.builder("o").argName("output")
                .hasArg().required().longOpt("output")
                .type(String.class)
                .desc("output folder to save model")
                .build();


        options.addOption(help);
        options.addOption(output_folder);
        if (false)
            options.getOptions().forEach(a -> {
                System.out.println(a.getArgName());
            });
        CommandLineParser parser = new DefaultParser();

        CommandLine line = null;
        try {
            line = parser.parse(options, argv);

        } catch (Exception e) {
            e.printStackTrace();
            printUsage(this.getClass().getName(), options);
            return null;
        }

        if (line.hasOption("help")) {
            printUsage(this.getClass().getName(), options);
            return null;
        }

        Args args = new Args();

        Utils.update_args(line, "lr", args, 0.1f);
        Utils.update_args(line, "epoch", args, 5);
        Utils.update_args(line, "dim", args, 100);
        Utils.update_args(line, "halfWindow", args, 5);
        Utils.update_args(line, "negSize", args, 5);
        Utils.update_args(line, "embeddingType", args);
        Utils.update_args(line, "thread", args, 12);
        Utils.update_args(line, "savePeriod", args, 2);
        Utils.update_args(line, "echoPeriod", args, 10000);
        Utils.update_args(line, "estimatedWordSize", args, 20_000_000);

        Utils.update_args(line, "address", args, "localhost");
        Utils.update_args(line, "queue", args, "QUEUE");
        Utils.update_args(line, "port", args, 6379);
        Utils.update_args(line, "adminPort", args, 10511);
        Utils.update_args(line, "epochSize", args);
        Utils.update_args(line, "minQueueSize", args, 5_000);

        args.put("output_folder", line.getOptionValue("output"));

        return args;
    }

    public static void printUsage(
            final String applicationName,
            final Options options) {
        final PrintWriter writer = new PrintWriter(System.err);
        final HelpFormatter usageFormatter = new HelpFormatter();
        usageFormatter.printUsage(writer, 80, applicationName, options);
        writer.flush();
    }

}
