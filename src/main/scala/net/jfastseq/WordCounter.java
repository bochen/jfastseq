package net.jfastseq;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class WordCounter implements Serializable {
    private static final long serialVersionUID = 54310864921548L;


    protected ConcurrentHashMap<Integer, Integer> table;
    private boolean allowInsert = true;

    public WordCounter(int initalize_size) {
        table = new ConcurrentHashMap<>(initalize_size);
    }

    public WordCounter() {

    }


    public void setAllowInsert(boolean b) {
        this.allowInsert = b;
    }

    public boolean getAllowInsert() {
        return this.allowInsert;
    }

    public Integer get(int word) {
        Integer v = table.get(word);
        if (v == null) {
            if (allowInsert) {
                create_row_if_not_exists(word);
                return table.get(word);
            } else {
                return null;
            }
        } else {
            return v;
        }

    }

    public void incr(int word) {
        if (table.containsKey(word)) {
            table.put(word, table.get(word) + 1);
        } else {
            if (allowInsert) {
                table.put(word, 1);
            }
        }
    }

    private synchronized void create_row_if_not_exists(int row) {
        if (!table.containsKey(row)) {
            table.put(row, 0);
        }
    }

    public int size() {
        return table.size();
    }

    public Collection<Integer> values() {
        return table.values();
    }

    public ConcurrentHashMap<Integer, Integer> getTable() {
        return table;
    }

    public void remove(Integer word) {
        table.remove(word);
    }

    public void touch(Integer word) {
        create_row_if_not_exists(word);
    }
}
