package net.jfastseq;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class LookupTable implements Serializable {
    private static final long serialVersionUID = 54348164921548L;
    private boolean allowInsert = true;

    protected int dim;

    protected ConcurrentHashMap<Integer, float[]> table;

    public LookupTable() {

    }

    public LookupTable(int dim, int initalize_size) {
        assert (dim > 0);
        this.dim = dim;
        table = new ConcurrentHashMap<>(initalize_size);
    }

    public LookupTable(int dim) {
        this(dim, 1000000);
    }

    public void setAllowInsert(boolean b) {
        this.allowInsert = b;
    }

    public boolean getAllowInsert() {
        return this.allowInsert;
    }

    public Float get(int row, int col) {
        float[] v = table.get(row);
        if (v == null) {
            if (allowInsert) {
                create_row_if_not_exists(row);
                return table.get(row)[col];
            } else {
                return null;
            }
        } else {
            return v[col];
        }

    }

    public float[] get(int row) {
        float[] v = table.get(row);
        if (v == null) {
            if (allowInsert) {
                this.create_row_if_not_exists(row);
                return table.get(row);
            } else {
                return null;
            }
        } else {
            return v;
        }
    }

    private synchronized void create_row_if_not_exists(int row) {
        if (!table.containsKey(row)) {
            float p = 1.0f / dim;
            float[] v = new float[this.dim];
            for (int i = 0; i < v.length; i++) {
                v[i] = p;
            }

            table.put(row, v);
        }
    }

    public void addRow(Vector grad, int word, float a) {
        float[] row = this.get(word);
        for (int j = 0; j < dim; j++) {
            row[j] += a * grad.get(j);
        }
    }

    public float dotRow(Vector vec, Integer word) throws Exception {
        float d = 0.0f;
        float[] row = get(word);
        for (int j = 0; j < dim; j++) {
            d += row[j] * vec.get(j);
        }
        if (Float.isNaN(d)) {
            throw new Exception("Encountered NaN.");
        }
        return d;
    }

    public int size() {
        return table.size();
    }

    public String shape() {
        return String.format("(%d,%d)", size(), dim);
    }

    public void remove(int word) {
        table.remove(word);
    }

    public boolean contains(int word) {
        if (allowInsert) {
            return true;
        } else {
            return table.containsKey(word);
        }
    }

    public List<Integer> getNanWords() {
        List<Integer> lst = new ArrayList<>();
        for (int key : table.keySet()) {
            if (hasNan(key)) {
                lst.add(key);
            }
        }
        return lst;
    }

    private boolean hasNan(int key) {
        for (float v : get(key)) {
            if (Float.isNaN(v)) {
                return true;
            }
        }
        return false;
    }
}
