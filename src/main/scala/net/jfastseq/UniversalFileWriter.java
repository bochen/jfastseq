package net.jfastseq;

import com.github.luben.zstd.ZstdOutputStream;

import java.io.*;
import java.util.zip.GZIPOutputStream;

public class UniversalFileWriter {

    private final BufferedWriter writer;
    private GZIPOutputStream gzipOutputStream = null;
    private String filename = null;
    private ZstdOutputStream zstdOutputStream = null;
    OutputStream writerOutputStream = null;
    FileOutputStream fileOutputStream = null;

    public UniversalFileWriter(String filename) throws IOException {
        this.filename = filename;
        if (filename == null || filename.equals("-")) {
            writerOutputStream = System.out;
        } else {

            fileOutputStream = new FileOutputStream(filename);
            if (filename.endsWith(".zst")) {
                zstdOutputStream = new ZstdOutputStream(fileOutputStream);
                writerOutputStream = zstdOutputStream;
            } else if (filename.endsWith(".gz")) {
                gzipOutputStream = new GZIPOutputStream(fileOutputStream);
                writerOutputStream = gzipOutputStream;
            } else {
                writerOutputStream = fileOutputStream;
            }

        }
        writer = new BufferedWriter(new OutputStreamWriter(writerOutputStream));

    }

    public void write(String text) throws IOException {
        writer.write(text);
    }

    public void writeLine(String text) throws IOException {
        writer.write(text);
        writer.write("\n");
    }

    public void close() {
        if (writer != null) {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (gzipOutputStream != null) {
            try {
                gzipOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (zstdOutputStream != null) {
            try {
                zstdOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (fileOutputStream != null) {
            try {
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }


}
