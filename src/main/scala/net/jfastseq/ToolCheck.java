package net.jfastseq;

import net.sparc.sparc;
import net.sparc.stringVector;
import org.apache.commons.cli.*;

import java.io.IOException;
import java.util.List;

public class ToolCheck {
    public ToolCheck() {
    }


    public void run_check(String[] argv) throws Exception {
        Option help = new Option("help", "print this message");
        Options options = new Options();
        Utils.create_option("inputModel", "input model to check", String.class, options, true);
        Utils.create_option("sup", "is supervised model (aka classifier)", Boolean.class, options, false);
        Utils.create_option("nan", "is supervised model (aka classifier)", Boolean.class, options, false);

        options.addOption(help);

        CommandLineParser parser = new DefaultParser();
        CommandLine cmdline = null;

        try {
            cmdline = parser.parse(options, argv);

        } catch (Exception e) {
            e.printStackTrace();
            Utils.printUsage(this.getClass().getName(), options);
            return;
        }

        if (cmdline.hasOption("help")) {
            Utils.printUsage(this.getClass().getName(), options);
            return;
        }

        String inputModel = Utils.getStringOption(cmdline, "inputModel");
        boolean is_sup = cmdline.hasOption("sup");
        boolean check_nan = cmdline.hasOption("nan");
        if (is_sup) {
            HSModel model = ContClassifier.makeModel(inputModel);
        } else {
            EmbeddingModel model = EmbeddingCont.makeModel(inputModel);
            if (check_nan) {
                List<Integer> lst = model.wi_.getNanWords();
                System.out.println("Input have " + lst.size() + " NaNs");
                for (int word : lst) {
                    System.out.println("Input NaN word: " + word);
                }
                lst = model.wo_.getNanWords();
                System.out.println("Output have " + lst.size() + " NaNs");
                for (int word : lst) {
                    System.out.println("Output NaN word: " + word);
                }

            }
        }
    }


}
