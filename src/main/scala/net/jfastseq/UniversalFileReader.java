package net.jfastseq;

import com.github.luben.zstd.ZstdInputStream;

import java.io.*;
import java.util.zip.GZIPInputStream;

public class UniversalFileReader implements  AutoCloseable {

    private final BufferedReader reader;
    private GZIPInputStream gzipInputStream = null;
    private String filename = null;
    private ZstdInputStream zstdInputStream = null;
    InputStream readerInputStream = null;
    FileInputStream fileInputStream = null;

    public UniversalFileReader(String filename) throws IOException {
        this.filename = filename;

        if (filename == null || filename.equals("-")) {
            readerInputStream = System.in;
        } else {

            fileInputStream = new FileInputStream(filename);
            if (filename.endsWith(".zst")) {
                zstdInputStream = new ZstdInputStream(fileInputStream);
                readerInputStream = zstdInputStream;
            } else if (filename.endsWith(".gz")) {
                gzipInputStream = new GZIPInputStream(fileInputStream);
                readerInputStream = gzipInputStream;
            } else {
                readerInputStream = fileInputStream;
            }

        }
        reader = new BufferedReader(new InputStreamReader(readerInputStream));

    }

    public String readLine() throws IOException {
        return reader.readLine();
    }

    public void close() {
        if (reader != null) {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (gzipInputStream != null) {
            try {
                gzipInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (zstdInputStream != null) {
            try {
                zstdInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (fileInputStream != null) {
            try {
                fileInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }
}
