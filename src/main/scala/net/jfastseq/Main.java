package net.jfastseq;

import java.util.Arrays;
import java.util.logging.Logger;

public class Main {
    private final static Logger logger = MyFormatter.getLogger(Main.class.getName());


    public static void main(String[] argv) throws Exception {
        if (argv.length < 1) {
            throw new Exception("use subcommands of train, resume, predict");
        }
        String command = argv[0];
        if (command.equals("train")) {
            new Trainer().run(Arrays.copyOfRange(argv, 1, argv.length));
        } else if (command.equals("resume")) {
            new ResumeTrainer().run(Arrays.copyOfRange(argv, 1, argv.length));
        } else if (command.equals("predict")) {
            new ToolClf().run_predict(Arrays.copyOfRange(argv, 1, argv.length));
        } else if (command.equals("cont-train")) {
            new ContTrainer().run(Arrays.copyOfRange(argv, 1, argv.length));
        } else if (command.equals("cont-train-resume")) {
            new ContResumeTrainer().run(Arrays.copyOfRange(argv, 1, argv.length));
        } else if (command.equals("cont-embedding")) {
            new EmbeddingContTrainer().run(Arrays.copyOfRange(argv, 1, argv.length));
        } else if (command.equals("cont-embedding-resume")) {
            new EmbeddingContResumeTrainer().run(Arrays.copyOfRange(argv, 1, argv.length));
        } else if (command.equals("embed")) {
            new ToolEmbedding().run_Embedding(Arrays.copyOfRange(argv, 1, argv.length));
        } else if (command.equals("check")) {
            new ToolCheck().run_check(Arrays.copyOfRange(argv, 1, argv.length));
        } else {
            throw new Exception("Unknown command: " + command);
        }


    }
}
