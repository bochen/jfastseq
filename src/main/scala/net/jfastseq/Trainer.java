package net.jfastseq;

import org.apache.commons.cli.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.logging.Logger;

public class Trainer {
    private final static Logger logger = MyFormatter.getLogger(Trainer.class.getName());

    public  void run(String[] argv) throws Exception {

        Args args = parse_args(argv);
        args.forEach((k, v) -> {
            logger.info(String.format("%s %s=%s", (v == null ? "null" : v.getClass().getName()), k, (v == null ? "null" : v.toString())));
        });
        logger.info(String.format("has %d input files", ((String[]) args.get("input_files")).length));

        if (args.input_files().length == 0) {
            throw new Exception("empty input");
        }
        new Classifier(args).train();
    }



    protected  Args parse_args(String[] argv) throws Exception {
        Option help = new Option("help", "print this message");

        Option debug = new Option("debug", "print debugging information");

        Options options = new Options();
        Utils.create_option("lr", "learning rate", Number.class, options);
        Utils.create_option("dim", "word dimension", Number.class, options);
        Utils.create_option("epoch", "training epoch", Number.class, options);
        Utils.create_option("thread", "number of threads", Number.class, options);
        Utils.create_option("estimatedWordSize", "estimatedWordSize", Number.class, options);
        Utils.create_option("estimatedLabelSize", "estimatedLabelSize", Number.class, options);
        Utils.create_option("savePeriod", "savePeriod", Number.class, options);
        Utils.create_option("echoPeriod", "echoPeriod", Number.class, options);
        Utils.create_option("tree", "taxonomy tree file", String.class, options);


        Option output_folder = Option.builder("o").argName("output")
                .hasArg().required().longOpt("output")
                .type(String.class)
                .desc("output folder to save model")
                .build();

        Option input_files = Option.builder("i").argName("input")
                .hasArgs().required().longOpt("input")
                .type(String[].class)
                .desc("input data files")
                .build();


        options.addOption(help);
        options.addOption(debug);
        options.addOption(output_folder);
        options.addOption(input_files);
        if (false)
            options.getOptions().forEach(a -> {
                System.out.println(a.getArgName());
            });
        CommandLineParser parser = new DefaultParser();
        CommandLine line = parser.parse(options, argv);

        Args args = new Args();

        Utils.update_args(line, "lr", args, 0.5f);
        Utils.update_args(line, "epoch", args, 5);
        Utils.update_args(line, "dim", args, 100);
        Utils.update_args(line, "thread", args, 12);
        Utils.update_args(line, "savePeriod", args);
        Utils.update_args(line, "echoPeriod", args);
        Utils.update_args(line, "estimatedWordSize", args);
        Utils.update_args(line, "estimatedLabelSize", args);

        args.put("tree", line.getOptionValue("tree"));
        args.put("output_folder", line.getOptionValue("output"));

        String[] patterns = line.getOptionValues("input");
        String[] files = Utils.findFiles(patterns);
        args.put("input_files", files);

        return args;
    }

}
