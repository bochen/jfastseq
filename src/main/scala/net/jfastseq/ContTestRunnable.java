package net.jfastseq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Response;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

public class ContTestRunnable extends Thread {

    private final ContClassifier classifier;
    private final HSModel model;
    private final Args args;
    private Logger logger;
    private boolean going_stop = false;

    public ContTestRunnable(ContClassifier classifier) {
        this.classifier = classifier;
        logger = LoggerFactory.getLogger(ContTestRunnable.class);

        int dim = classifier.getArgs().getDim();
        this.args = classifier.getArgs();
        long echo_per_n_read = args.getEchoPeriod();
        this.model = new HSModel(dim, classifier.getInputWeights(), classifier.getOutputWeights(), classifier.getTree(), classifier.word_counts);
    }

    public void run() {
        String host = args.getRedisHost();
        int port = args.getRedisPort();
        String queue = args.getQueueName();
        byte[] queueBytes = queue.getBytes();
        byte[] testQueueBytes = (queue + ".test").getBytes();
        logger.info(String.format("connect to %s:%d, use queue: %s", host, port, queue));
        Jedis jedis = new Jedis(host, port);
        ArrayList<Float> loss_history = new ArrayList<>();
        ArrayList<Float> accur1_history = new ArrayList<>();
        ArrayList<Float> accur3_history = new ArrayList<>();
        ArrayList<Float> accur5_history = new ArrayList<>();
        ArrayList<Float> accur10_history = new ArrayList<>();
        int max_k = 10;
        boolean[] arr_bool = new boolean[max_k];
        long total_num = 0;
        while (!going_stop) {
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (!classifier.thread_running) {
                continue;
            }
            Pipeline p = jedis.pipelined();
            Response<Set<byte[]>> rep1 = p.spop(testQueueBytes, 10);
            Response<Long> rep2 = p.scard(queueBytes);
            p.sync();
            classifier.queueSize = rep2.get();
            Set<byte[]> ret = rep1.get();
            ReadMsgProtos.Read read = null;

            if (ret.size() < 5) {
                try {
                    logger.info("Starving...");
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                float loss = 0f;
                int n_acc1 = 0;
                int n_acc3 = 0;
                int n_acc5 = 0;
                int n_acc10 = 0;
                for (byte[] bytes : ret) {
                    try {
                        read = ReadMsgProtos.Read.parseFrom(bytes);
                        loss += model.calcLoss(read);

                        int label = read.getLabel();
                        HSModel.PredPair[] predicts = model.predict(read, max_k, 0.001f, true, false, label);
                        //System.err.println("" + label + " " + Arrays.toString(predicts));
                        for (int i = 0; i < max_k; i++) {
                            arr_bool[i] = false;
                        }
                        for (int i = 0; i < predicts.length; i++) {
                            arr_bool[i] = label == predicts[i].label;
                        }
                        n_acc1 += is_true(arr_bool, 1);
                        n_acc3 += is_true(arr_bool, 3);
                        n_acc5 += is_true(arr_bool, 5);
                        n_acc10 += is_true(arr_bool, 10);

                    } catch (Exception e) {
                        System.err.println(Arrays.toString(bytes));
                        if (read != null) {
                            System.err.println(read.toString());
                        }
                        e.printStackTrace();
                    }

                }
                total_num++;
                if (!ret.isEmpty()) {
                    float meanloss = loss / ret.size();
                    loss_history.add(meanloss);
                    float acc1 = ((float) n_acc1) / ret.size();
                    accur1_history.add(acc1);
                    float acc3 = ((float) n_acc3) / ret.size();
                    accur3_history.add(acc3);
                    float acc5 = ((float) n_acc5) / ret.size();
                    accur5_history.add(acc5);
                    float acc10 = ((float) n_acc10) / ret.size();
                    accur10_history.add(acc10);
                    System.err.println(String.format("Test_loss:\t%d\t%.3f\tacc[1,3,5,10]\t%.3f\t%.3f\t%.3f\t%.3f",
                            System.currentTimeMillis(), meanloss, acc1, acc3, acc5, acc10));
                    if (total_num > 0 && total_num % 60 == 0) {
                        float mean_minutes = mean_loss(loss_history, 60);
                        float mean_15_minutes = mean_loss(loss_history, 60 * 15);
                        float mean_30_minutes = mean_loss(loss_history, 60 * 30);
                        float mean_60_minutes = mean_loss(loss_history, 60 * 60);
                        float mean_120_minutes = mean_loss(loss_history, 60 * 120);
                        float mean_600_minutes = mean_loss(loss_history, 60 * 600);
                        System.err.println(String.format("History_loss: min %.3f, 15min %.3f, 30min %.3f, 1hr %.3f, 2hr %.3f, 5hr %.3f",
                                mean_minutes, mean_15_minutes, mean_30_minutes, mean_60_minutes, mean_120_minutes, mean_600_minutes));
                    }

                    if (total_num > 0 && total_num % 60 == 0) {
                        float mean_minutes = mean_loss(accur1_history, 60);
                        float mean_15_minutes = mean_loss(accur1_history, 60 * 15);
                        float mean_30_minutes = mean_loss(accur1_history, 60 * 30);
                        float mean_60_minutes = mean_loss(accur1_history, 60 * 60);
                        float mean_120_minutes = mean_loss(accur1_history, 60 * 120);
                        float mean_600_minutes = mean_loss(accur1_history, 60 * 600);
                        System.err.println(String.format("History_acc1: min %.3f, 15min %.3f, 30min %.3f, 1hr %.3f, 2hr %.3f, 5hr %.3f",
                                mean_minutes, mean_15_minutes, mean_30_minutes, mean_60_minutes, mean_120_minutes, mean_600_minutes));
                    }

                    if (total_num > 0 && total_num % 60 == 0) {
                        float mean_minutes = mean_loss(accur3_history, 60);
                        float mean_15_minutes = mean_loss(accur3_history, 60 * 15);
                        float mean_30_minutes = mean_loss(accur3_history, 60 * 30);
                        float mean_60_minutes = mean_loss(accur3_history, 60 * 60);
                        float mean_120_minutes = mean_loss(accur3_history, 60 * 120);
                        float mean_600_minutes = mean_loss(accur3_history, 60 * 600);
                        System.err.println(String.format("History_acc3: min %.3f, 15min %.3f, 30min %.3f, 1hr %.3f, 2hr %.3f, 5hr %.3f",
                                mean_minutes, mean_15_minutes, mean_30_minutes, mean_60_minutes, mean_120_minutes, mean_600_minutes));
                    }

                    if (total_num > 0 && total_num % 60 == 0) {
                        float mean_minutes = mean_loss(accur5_history, 60);
                        float mean_15_minutes = mean_loss(accur5_history, 60 * 15);
                        float mean_30_minutes = mean_loss(accur5_history, 60 * 30);
                        float mean_60_minutes = mean_loss(accur5_history, 60 * 60);
                        float mean_120_minutes = mean_loss(accur5_history, 60 * 120);
                        float mean_600_minutes = mean_loss(accur5_history, 60 * 600);
                        System.err.println(String.format("History_acc5: min %.3f, 15min %.3f, 30min %.3f, 1hr %.3f, 2hr %.3f, 5hr %.3f",
                                mean_minutes, mean_15_minutes, mean_30_minutes, mean_60_minutes, mean_120_minutes, mean_600_minutes));
                    }

                    if (total_num > 0 && total_num % 60 == 0) {
                        float mean_minutes = mean_loss(accur10_history, 60);
                        float mean_15_minutes = mean_loss(accur10_history, 60 * 15);
                        float mean_30_minutes = mean_loss(accur10_history, 60 * 30);
                        float mean_60_minutes = mean_loss(accur10_history, 60 * 60);
                        float mean_120_minutes = mean_loss(accur10_history, 60 * 120);
                        float mean_600_minutes = mean_loss(accur10_history, 60 * 600);
                        System.err.println(String.format("History_acc10: min %.3f, 15min %.3f, 30min %.3f, 1hr %.3f, 2hr %.3f, 5hr %.3f",
                                mean_minutes, mean_15_minutes, mean_30_minutes, mean_60_minutes, mean_120_minutes, mean_600_minutes));
                    }

                }
            }


        }
    }

    private int is_true(boolean[] arr, int k) {
        for (int i = 0; i < k; i++) {
            if (arr[i]) {
                return 1;
            }
        }
        return 0;
    }

    private float mean_loss(ArrayList<Float> loss_history, int last_n) {
        if (loss_history.size() < last_n) {
            return -1f;
        } else {
            float loss = 0f;
            for (int i = loss_history.size() - last_n; i < loss_history.size(); i++) {
                loss += loss_history.get(i);
            }
            return loss / last_n;

        }
    }

    public void setStop(boolean b) {
        this.going_stop = b;
    }
}