package net.jfastseq;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.github.luben.zstd.ZstdInputStream;
import com.github.luben.zstd.ZstdOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class Classifier {

    private final static  Logger logger = LoggerFactory.getLogger(Classifier.class);
    private boolean trainable = true;
    protected WordCounter word_counts;
    private Args args;
    private LookupTable input_weights;
    private LookupTable output_weights;
    private Tree tree;
    private int current_epoch = 0;

    private final static Kryo kryo = new Kryo();

    static {
        kryo.register(String[].class);
        kryo.register(float[].class);
        kryo.register(HashMap.class);
        kryo.register(java.util.LinkedHashMap.class);
        kryo.register(ConcurrentHashMap.class);

        kryo.register(SaverObj.class);
        kryo.register(Args.class);
        kryo.register(LookupTable.class);
        kryo.register(WordCounter.class);
        kryo.register(Tree.class);
        kryo.register(TreeNode.class);
    }


    public static HSModel makeModel(String modelFile) throws Exception {
        Classifier clf = new Classifier(modelFile);
        HSModel model = new HSModel(clf.args.getDim(), clf.getInputWeights(), clf.getOutputWeights(), clf.getTree(), null);
        return model;

    }

    public Classifier(String aModelFile) throws Exception {
        this.trainable = false;
        this.load(aModelFile);
        //this.tree.build();
    }

    public Classifier(Args args) throws Exception {
        String resume = args.getResumeModel();
        if (!resume.isEmpty()) {
            this.load(resume);
            this.tree.build();
            if (args.get("thread") != null) {
                this.args.put("thread", args.getThread());
            }
            if (args.get("input_files") != null) {
                this.args.put("input_files", args.get("input_files"));
            }
            this.args.forEach((k, v) -> {
                logger.info(String.format("%s %s=%s", (v == null ? "null" : v.getClass().getName()), k, (v == null ? "null" : v.toString())));
            });
            checkArgs(true);
        } else {
            this.args = args;
            checkArgs(false);

            String output = (String) args.get("output_folder");
            Utils.create_folder(output);
            int dim = args.getDim();
            word_counts = new WordCounter(args.getEstimatedWordSize());
            input_weights = new LookupTable(dim, args.getEstimatedWordSize());
            this.output_weights = new LookupTable(dim, args.getEstimatedLabelSize());
            String tree_file = (String) args.get("tree");
            logger.info("Building tree ...");
            tree = new Tree(tree_file);
            logger.info("Tree length: " + tree.size());
            logger.info("Finish building tree ");
        }
    }


    private void checkArgs(boolean resume) throws Exception {
        String[] files = (String[]) args.get("input_files");
        for (int i = 0; i < files.length; i++) {
            if (!Utils.path_exists(files[i], false)) {
                throw new Exception("file not exists: " + files[i]);
            }
        }
        String output = (String) args.get("output_folder");
        if (!resume && Utils.path_exists(output, true)) {
            throw new Exception("output fold exists: " + output);
        }

        if (!resume) {
            String tree_file = (String) args.get("tree");
            if (!Utils.path_exists(tree_file, false)) {
                throw new Exception("no tree file found:  " + tree_file);
            }
        }
    }

    public void train() throws Exception {
        if (!this.trainable) {
            throw new RuntimeException("model is not trainable");
        }
        if (current_epoch==0) {
            save_model(-1);
        }
        int n_epoch = args.getEpoch();
        for (int i = current_epoch; i < n_epoch; i++) {
            logger.info("start epoch " + i);
            start_threads(i);
            logger.info("checkpoint epoch " + i);
            current_epoch = i + 1;
            if (i + 1 == n_epoch || i % args.getSavePeriod() == 0) {
                save_model(i);
            }
        }
    }

    private void load(String modelpath) throws IOException {
        // Reading the object from a file
        logger.info("read model from " + modelpath);
        FileInputStream file = new FileInputStream(modelpath);
        ZstdInputStream zis = new ZstdInputStream(file);

        Input input = new Input(zis);
        SaverObj objects = kryo.readObject(input, SaverObj.class);

        this.current_epoch = objects.current_epoch;
        this.args = objects.args;
        this.tree = objects.tree;
        this.word_counts = objects.word_counts;
        this.input_weights = objects.input_weights;
        this.output_weights = objects.output_weights;

        input.close();
        zis.close();
        file.close();
        logger.info("finish reading model from  " + modelpath);
    }

    private void save_model(int epoch) throws Exception {
        String output = (String) args.get("output_folder");
        String output_file = Paths.get(output, "model_" + epoch).toAbsolutePath().toString();
        logger.info("writing model to " + output_file);
        //Saving of object in a file
        FileOutputStream file = new FileOutputStream(output_file);
        ZstdOutputStream zos = new ZstdOutputStream(file);
        Output okyro = new Output(zos);

        // Method for serialization of object
        SaverObj objects = new SaverObj();
        int i = 0;
        objects.current_epoch = (this.current_epoch);
        objects.args = (this.args);
        objects.tree = (this.tree);
        objects.word_counts = (this.word_counts);
        objects.input_weights = (this.input_weights);
        objects.output_weights = (this.output_weights);
        kryo.writeObject(okyro, objects);

        okyro.close();
        zos.close();
        file.close();
        logger.info("finish writing " + output_file);

    }

    public LookupTable getInputWeights() {
        return input_weights;
    }

    public LookupTable getOutputWeights() {
        return output_weights;
    }

    private void start_threads(int epoch) throws InterruptedException {
        int n_thread = args.getThread();

        String[] files = (String[]) args.get("input_files");
        ArrayUtils.shuffleArray(files);
        if (files.length < n_thread) {
            logger.warn("thread number > input files");
        }

        int n_epoch = args.getEpoch();
        float lr = args.getLearningRate();
        double lr_start = (1.0 - 1.0 * epoch / n_epoch) * lr;
        logger.info(String.format("epoch %d, learning rate: %f", epoch, lr_start));
        ArrayList<Thread> threads = new ArrayList<>();
        for (int i = 0; i < n_thread; i++) {
            ArrayList<String> local_files = new ArrayList<>();
            for (int j = 0; j < files.length; j++) {
                if (j % n_thread == i) {
                    local_files.add(files[j]);
                }
            }
            Thread thread = new Thread(new TrainRunnable(i, this, lr_start, local_files, epoch == 0));
            threads.add(thread);
            thread.start();
        }


        for (int i = 0; i < threads.size(); i++) {
            threads.get(i).join();
        }
    }


    public Tree getTree() {
        return this.tree;
    }

    public Args getArgs() {
        return args;
    }
}
