package net.jfastseq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;

import java.util.Arrays;
import java.util.Set;

public class EmbeddingContTrainRunnable extends Thread {

    private final int tid;
    private final EmbeddingCont classifier;
    private final float lr;
    private final EmbeddingModel model;
    private final Args args;
    private final int epochSize;

    public int num_total_processed_reads;
    private Logger logger;
    private long echo_per_n_read;

    public EmbeddingContTrainRunnable(int tid, EmbeddingCont classifier, double lr, int epochSize, boolean update_wc) {
        this.tid = tid;
        boolean update_wc1 = update_wc;
        this.classifier = classifier;
        this.epochSize = epochSize;
        this.lr = (float) lr;
        logger = LoggerFactory.getLogger(EmbeddingContTrainRunnable.class);

        int dim = classifier.getArgs().getDim();
        this.args = classifier.getArgs();
        echo_per_n_read = args.getEchoPeriod();
        WordCounter wc = update_wc ? classifier.word_counts : null;
        if (!update_wc) {
            logger.info("word counting is disabled.");
        } else {
            logger.info("word counting is enabled.");
        }
        logger.info("epochSize=" + epochSize);
        this.model = new EmbeddingModel
                (dim, classifier.getInputWeights(),
                classifier.getOutputWeights(), args.getNegSize(), wc, classifier.getNegatives(), args.getHalfWindow(), args.useCbow());
    }

    public void run() {
        String host = args.getRedisHost();
        int port = args.getRedisPort();
        String queue = args.getQueueName();
        byte[] queueBytes = queue.getBytes();
        logger.info(String.format("connect to %s:%d, use queue: %s", host, port, queue));
        Jedis jedis = new Jedis(host, port);

        long startTime = System.nanoTime();
        long skipped = 0;
        boolean isrcc = args.is_rcc();
        long run_start_time = System.nanoTime();
        int queue_thread_shold=args.getMinQueueSize();
        for (; ; ) {
            long curr_time = System.nanoTime();
            if (!isrcc && num_total_processed_reads >= epochSize) {
                break;
            } else if (isrcc && curr_time - run_start_time >= (3L * 3600 + 30 * 60) * 1_000_000_000) {
                break;

            }
            if(queue_thread_shold>classifier.queueSize){
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                continue;
            }

            Set<byte[]> ret = jedis.spop(queueBytes, 100);
            ReadMsgProtos.Read read;
            if (ret.isEmpty()) {
                try {
                    logger.info("Starving...");
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else
                for (byte[] bytes : ret) {
                    try {
                        read = ReadMsgProtos.Read.parseFrom(bytes);
                        model.update(read, this.lr);
                        num_total_processed_reads++;

                        if (tid == 0 && num_total_processed_reads % echo_per_n_read == 0 && num_total_processed_reads > 0) {
                            long endTime = System.nanoTime();
                            double duration = (endTime - startTime) * 1e-9;
                            double avg_rate = num_total_processed_reads / duration;
                            System.err.println(String.format("Progress: %.0f words/thread/second, loss=%.3f, lr=%.5f",
                                    avg_rate, model.avgLoss(), lr));
                            model.zeroLoss();
                        }
                    } catch (Exception e) {
                        skipped++;
                        System.err.println(Arrays.toString(bytes));
                        e.printStackTrace();
                    }

                }

        }
        System.err.println();
        logger.info(String.format("finish training %d records, skipping %d lines", num_total_processed_reads, skipped));
    }
}