package net.jfastseq;

public class Vector {
    private final int dim;
    private float[] data;

    public Vector(int dim) {
        this.dim = dim;
        data = new float[dim];
    }

    public void zero() {
        for (int i = 0; i < data.length; i++) {
            data[i] = 0f;
        }
    }

    public void addRow(LookupTable A, int word) {
        for (int j = 0; j < dim; j++) {
            data[j] += A.get(word, j);
        }
    }

    public void mul(float a) {
        for (int j = 0; j < dim; j++) {
            data[j] *= a;
        }
    }

    public float get(int j) {
        return data[j];
    }

    public void addRow(LookupTable A, Integer word, float a) {
        for (int j = 0; j < dim; j++) {
            data[j] += a * A.get(word, j);
        }
    }

    public int size() {
        return data.length;
    }

    public void divide(float a) {
        for (int j = 0; j < dim; j++) {
            data[j] /= a;
        }
    }
}
